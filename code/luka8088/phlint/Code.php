<?php

namespace luka8088\phlint;

use \ArrayObject;
use \luka8088\phlint\inference;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\Test as PhlintTest;
use \ReflectionMethod;

class Code {

  /** @internal */
  public $inferences = [];

  /** @internal */
  public $autoloaders = [];

  /** @internal */
  public $autoloadLookups = [];

  /** @internal */
  public $globalVariables = [];

  /** @internal */
  public $scopes = [];

  public $asts = [];
  public $astsIsLibrary = [];

  public $loadedFileMap = [];

  public $symbols = [];

  public $imports = [];

  public $interfaceSymbolMap = [];

  function __construct () {
    $this->globalVariables = new ArrayObject();
  }

  function addAst ($ast, $isLibrary = false) {

    if (count($ast) == 0 || !$ast[0]->getAttribute('inferred'))
      Code::infer($ast, $this->inferences);

    $this->asts[] = $ast;
    $this->astsIsLibrary[] = $isLibrary;
  }

  function load ($code) {

    $expandedCode = [];

    #context('output')->__invoke("Collecting code files ...\n");

    foreach ($code as $codeEntry) {

      if ($codeEntry['source']) {
        $codeEntry['source'] = preg_replace('/(?is)\A\r?\n/', '', $codeEntry['source']);
        $codeEntry['source'] = (substr($codeEntry['source'], 0, 2) != '<?' ? '<?php ' : '') . $codeEntry['source'];
        $expandedCode[] = $codeEntry;
        continue;
      }

      if (is_file($codeEntry['path'])) {
        $iterator = [$codeEntry['path']];
      } else {
        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($codeEntry['path']));
        $iterator = new \RegexIterator($iterator, '/(?i)\.php$/');
      }

      foreach ($iterator as $file) {
        $filePath = is_object($file) ? $file->getPathName() : $file;
        context('code')->loadedFileMap[$filePath] = true;
        context('code')->loadedFileMap[realpath($filePath)] = true;
        $expandedCode[] = [
          'path' => $file,
          'source' => file_get_contents($file),
          'isLibrary' => $codeEntry['isLibrary'],
        ];
      }

    }

    #context('output')->__invoke("Parsing code ...\n");

    $aggregatedAst = [];

    foreach ($expandedCode as &$codeEntry) {
      try {
        if (false)
        if ($codeEntry['path'])
          context('output')->__invoke(($codeEntry['isLibrary'] ? '  Library: ' : '') . $codeEntry['path'] . "\n");
        $codeEntry['ast'] = self::parse($codeEntry['source']);
        if ($codeEntry['path'])
          self::populatePath($codeEntry['ast'], $codeEntry['path']);
        if ($codeEntry['isLibrary'])
          NodeTraverser::traverse($codeEntry['ast'], [function ($node) { $node->setAttribute('isLibrary', true); }]);
        $aggregatedAst = array_merge($aggregatedAst, $codeEntry['ast']);
      } catch (\PhpParser\Error $exception) {
        context('result')->addIssue(null, 'Parse error: ' . $exception->getMessage()
          . ($codeEntry['path'] ? ' in *' . realpath($codeEntry['path']) . '*.' : '.')
        );
      }
    }

    self::infer($aggregatedAst, $this->inferences);

    foreach ($expandedCode as &$codeEntry) {
      if (!isset($codeEntry['ast']))
        continue;
      $this->addAst($codeEntry['ast'], $codeEntry['isLibrary']);
    }

  }

  static function infer ($ast, $inferrers = []) {

    if (count($inferrers) == 0)
      $inferrers = context('code')->inferences;

    if (count($ast) > 0)
      $ast[0]->setAttribute('inferred', true);

    #context('output')->__invoke("Inferring about code ...\n");

    #context('output')->indent();

    $inferrerPasses = [];

    foreach ($inferrers as $inferrer) {

      $pass = 10;

      if (is_callable($inferrer) && (!is_object($inferrer) || ($inferrer instanceof \Closure))) {
        $reflection = new ReflectionMethod($inferrer);
        if (preg_match('/(?is)\@pass[ \t\r\n]*\([ \t\r\n]*([^\)]+)\)/', $reflection->getDocComment(), $match))
          $pass = $match[1];
      }

      if (method_exists($inferrer, 'getPass'))
        $pass = $inferrer->getPass();

      // Allow running the same inference in different passes.
      // @todo: Rethink.
      foreach ((array) $pass as $p) {
        if (!isset($inferrerPasses[$p]))
          $inferrerPasses[$p] = [];
        $inferrerPasses[$p][] = $inferrer;
      }

    }

    ksort($inferrerPasses);

    foreach ($inferrerPasses as $pass => $inferrers) {
      #context('output')->__invoke("Running inference pass " . $pass . " ...\n");
      $inferrersx = [];
      foreach ($inferrers as $inferrer)
        $inferrersx[] = clone $inferrer;
      NodeTraverser::traverse($ast, $inferrersx);
    }

    #context('output')->unindent();

    #context('output')->__invoke("Code inference complete.\n");

    return $ast;

  }

  static function analyze ($ast, $rules) {
    NodeTraverser::traverse($ast, $rules);
  }

  static function parse ($source, $path = '') {

    $parserFactory = new \PhpParser\ParserFactory();
    $parser = $parserFactory->create(\PhpParser\ParserFactory::PREFER_PHP7);

    $ast = $parser->parse(preg_replace('/(?is)\A\<\?(?=[ \t\r\n])/', '<?php', $source));

    if (preg_match('/(?is)\A\<\?(?=[ \t\r\n])/', $source) > 0 && count($ast) > 0)
      $ast[0]->setAttribute('hasShortOpenTag', true);

    NodeTraverser::traverse($ast, [function ($node) {
      $node->setAttribute('isSourceAvailable', true);
    }]);

    return $ast;

  }

  protected static function populatePath ($ast, $path) {
    NodeTraverser::traverse($ast, [function ($node) use ($path) {
      $node->setAttribute('path', $path);
    }]);
  }

  function registerSymbolLink ($symbol, $node) {

    $symbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $symbol);

    assert(is_string($symbol) && $symbol != '');
    assert(strpos($symbol, '.') !== 0);

    if (!isset($this->symbols[$symbol]))
      $this->symbols[$symbol] = [
        'id' => $symbol,
        'phpId' => '',
        'aliasOf' => '',
        'definitionNodes' => [],
        'linkedNodes' => [],
      ];

    if (!isset($this->symbols[$symbol]['linkedNodes']))
      $this->symbols[$symbol]['linkedNodes'] = [];

    if ($node) {

      $isNodeAttached = false;
      foreach ($this->symbols[$symbol]['linkedNodes'] as $existingLinkedNode)
        if ($existingLinkedNode === $node)
          $isNodeAttached = true;
      if (!$isNodeAttached)
        $this->symbols[$symbol]['linkedNodes'][] = $node;

      if (!$this->symbols[$symbol]['phpId']) {

        $phpId = $this->symbols[$symbol]['phpId'];

        /**
         * Added only for optimization purposes.
         * Namespaces in PHP are always top level.
         */
        if (!$phpId && ($node instanceof Node\Stmt\Namespace_))
          $phpId = inference\Symbol::name($node);

        if (!$phpId) {
          $namedScope = $symbol;
          while ($namedScope) {
            $namedScope = inference\Scope::namedAncestor($namedScope);
            if (isset($this->symbols[$namedScope]))
              break;
          }
          $phpId = ($namedScope ? $this->symbols[$namedScope]['phpId'] : '') . '\\' . inference\Symbol::name($node);
        }

        $this->symbols[$symbol]['phpId'] = ltrim($phpId, '\\');

      }

    }

  }

  function isImplicitlyConvertible ($type1, $type2, $redundants = []) {
    if ($type1 == $type2)
      return true;
    if (!isset($this->symbols[$type1]))
      return false;
    if (!isset($this->symbols[$type1]['implicitlyConvertable']))
      return false;
    foreach (array_diff($this->symbols[$type1]['implicitlyConvertable'], $redundants) as $implicitlyConvertableType)
      if ($this->isImplicitlyConvertible(
            $implicitlyConvertableType,
            $type2,
            array_merge($redundants, $this->symbols[$type1]['implicitlyConvertable'])
          ))
        return true;
    return false;
  }

  /**
   * Sanity test of some edge case situations to make sure
   * they don't cause troubles.
   *
   * @test @internal
   */
  static function unittest_sanityTest () {

    PhlintTest::assertIssues('
      function f () {
        $x =
      }
    ', [
      'Parse error: Syntax error, unexpected \'}\' on line 3.',
    ]);

    PhlintTest::assertNoIssues('

      function f () {
        if (rand(0, 1))
          $x = 0;
        else
          $o = function () {};
      }

      function f2 () {}

      class C {
        function m () {}
      }

      interface I {
        function f ();
      }

    ');
  }

}
