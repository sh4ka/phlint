<?php

namespace luka8088\phlint;

use \ArrayObject;
use \Exception;
use \luka8088\Phlint;
use \luka8088\phlint\Code;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeTraverser;
use \PhpParser\Comment\Doc as DocComment;
use \PhpParser\Node\Expr\ConstFetch;
use \PhpParser\Node\Name as NameNode;
use \PhpParser\Node\Param;
use \PhpParser\Node\Scalar\LNumber;
use \PhpParser\Node\Scalar\String_;
use \PhpParser\Node\Stmt\Class_;
use \PhpParser\Node\Stmt\ClassMethod;
use \PhpParser\Node\Stmt\Function_;
use \ReflectionClass;
use \ReflectionFunction;
use \SimpleXMLElement;

/**
 * This class handles PHP internal and standard library information that
 * is not available out of the box or needs to be available in a
 * specific format.
 */
class Internal {

  static $superglobals = [
    /** @var array */ '_COOKIE' => [],
    /** @var array */ '_ENV' => [],
    /** @var array */ '_FILES' => [],
    /** @var array */ '_GET' => [],
    /** @var array */ '_POST' => [],
    /** @var array */ '_REQUEST' => [],
    /** @var array */ '_SERVER' => [],
    /** @var array */ '_SESSION' => [],
    /** @var array */ 'GLOBALS' => [],
  ];

  /**
   * @see http://php.net/manual/en/functions.arguments.php#functions.arguments.type-declaration.types
   */
  static $typeDeclarationNonClassKeywords = [
    'array',
    'bool',
    'callable',
    'float',
    'int',
    'string',
  ];

  static function documentationPath () {
    return __dir__ . '/../../../data/php-documentation.html';
  }

  static function definitionPath () {
    return __dir__ . '/../../../data/import/';
  }

  static function importDefinitions ($id) {

    static $cache = [];

    if (!isset($cache[$id])) {

      if (!is_file(__dir__ . '/../../../data/import/php-' . $id . '.php'))
        throw new Exception('Import file *' . __dir__ . '/../../../data/import/php-' . $id . '.php' . '* not found.');

      $cache[$id] = Code::parse(file_get_contents(__dir__ . '/../../../data/import/php-' . $id . '.php'));

      NodeTraverser::traverse($cache[$id], [function ($node) {
        $node->setAttribute('isSourceAvailable', false);
        $node->setAttribute('startLine', 0);
        $node->setAttribute('endLine', 0);
      }]);

      #$code = file_get_contents(__dir__ . '/../../../data/import/php-' . $id . '.php');
      #
      #$sharedCacheKey = 'phlint_code_ast_aGLKTqGoWnJS2AClL4eaJ9Le_' . sha1($code);
      #
      #if (!apcu_exists($sharedCacheKey)) {
      #  apcu_store($sharedCacheKey, serialize(Code::parse($code)));
      #}
      #
      #$cache[$id] = unserialize(apcu_fetch($sharedCacheKey));

      #$cache[$id] = Code::parse($code);

    }

    // @todo: Clone.
    return $cache[$id];

    $import = '';
    foreach (glob(__dir__ . '/../../../data/import/php-*.php') as $importFile) {
      #try {
      #  Code::parse(file_get_contents($importFile));
      #} catch (\Throwable $t) {
      #  var_dump($importFile);
      #  exit;
      #}
      $import .= substr(file_get_contents($importFile), 5) . "\n";
    }
    return '<?php ' . $import;
  }

  static function downloadPhpDocumentation () {
    $curl = curl_init();

    // Todo: Only in development.
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

    curl_setopt($curl, CURLOPT_URL, 'http://php.net/get/php_manual_en.html.gz/from/this/mirror');

    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $content = curl_exec($curl);

    $documentation = gzdecode($content);

    file_put_contents(self::documentationPath(), $documentation);

  }

  static function generateDefinitions () {

    #var_dump(Code::parse('<?php class z { function a () {} function b () {} }'));
    #exit;

    $definitions = [];

    #$definitions = self::mergeDefinitions(
    #  self::extractReflectionDefinitions(),
    #  self::extractReflectionDefinitions()#,
    #  #[]
    #);

    #var_dump($definitions['f_exif_read_data']);
    #exit;

    $definitions = self::mergeDefinitions(
      self::extractReflectionDefinitions(),
      self::extractDocumentationDefinitions(),
      []
    );

    #var_dump($definitions['f_exif_read_data']);
    #exit;

    $output = [
      'php-standard' => '',
    ];

    $superglobals = self::$superglobals;

    ksort($superglobals);

    foreach ($superglobals as $superglobal => $initial) {
      $output['php-standard'] .= '/** @var array */ $' . $superglobal . ' = [];';
      $output['php-standard'] .= "\n";
    }

    $output['php-standard'] .= "\n\n";

    foreach ($definitions as $definition) {
      echo '1';
      self::patchDefinition($definition);
    }

    uksort($definitions, function ($a, $b) {
      if (strpos($a, 'f_') === 0)
        $a = '10_' . $a;
      if (strpos($b, 'f_') === 0)
        $b = '10_' . $b;
      if (strpos($a, 'c_') === 0)
        $a = '20_' . $a;
      if (strpos($b, 'c_') === 0)
        $b = '20_' . $b;
      return strcmp(strtolower($a), strtolower($b));
    });

    foreach ($definitions as $definition) {
      echo '2';

      $serializedDefinition = NodeConcept::sourcePrint($definition);

      if ($definition instanceof Function_) {
        #if (!function_exists($definition->name)) {
        #  continue;
        #}
        $serializedDefinition = trim(preg_replace('/(?s)[ \t\r\n]+/', ' ', $serializedDefinition));
        $serializedDefinition = str_replace('{ }', '{}', $serializedDefinition);
      }

      if ($definition instanceof Class_) {
        #if (!class_exists($definition->name)) {
        #  continue;
        #}
        $serializedDefinition = preg_replace('/(?si)(function[^\{\r\n]*)[ \t\r\n]*\{[ \t\r\n]*\}/', '$1 {}', $serializedDefinition);
        $serializedDefinition = "\n" . $serializedDefinition;
      }

      $sourceLibrary = $definition->getAttribute('source-library');

      if (!$sourceLibrary)
        continue;

      #if (!$sourceLibrary)
      #  $sourceLibrary = 'php-standard';

      if (!isset($output[$sourceLibrary])) {
        $output[$sourceLibrary] = '';
      }

      $output[$sourceLibrary] .= $serializedDefinition . "\n";
    }

    // Make sure that output is parsable.
    // Todo: Remove
    try {
    foreach ($output as $outputChunk)
      Code::parse($outputChunk);
    } catch (\Exception $e) {
      echo $e;
      echo $output;
    }

    foreach ($output as $sourceLibrary => $outputChunk) {
      file_put_contents(self::definitionPath() . '/' . $sourceLibrary . '.php', "<?php\n\n" . $outputChunk);
    }
    #$output = []'<?php';
    #$output .= "\n\n";


    #file_put_contents(self::definitionPath(), $output);

  }

  static function mergeDefinitions ($a, $b) {

    if (count(func_get_args()) > 2)
      return self::mergeDefinitions($a, call_user_func_array(
        [__class__, 'mergeDefinitions'],
        array_slice(func_get_args(), 1)
      ));

    $mergeSingleDefinition = function ($a, $b) {

      if ($b instanceof Function_) {

        #var_dump($a);
        #exit;

        $a->byRef = $b->byRef;

        if ($b->name)
          $a->name = $b->name;

        foreach ($b->params as $offset => $argument) {

          if (!isset($a->params[$offset]))
            $a->params[$offset] = $argument;

          if ($argument->type)
            $a->params[$offset]->type = $argument->type;

          $a->params[$offset]->byRef = $argument->byRef;

          $a->params[$offset]->variadic = $argument->variadic;

          if ($argument->name)
            $a->params[$offset]->name = $argument->name;

          $a->params[$offset]->default = $argument->default;

          foreach ($argument->getAttributes() as $attributeName => $attributeValue)
            $a->params[$offset]->setAttribute($attributeName, $attributeValue);

        }

        if ($b->returnType)
          $a->returnType = $b->returnType;

        foreach ($b->getAttributes() as $attributeName => $attributeValue)
          $a->setAttribute($attributeName, $attributeValue);

      }

      if ($b instanceof Class_) {
        #var_dump($a);
        #exit;
        // @todo: Implement.
        $a = $b;
      }

      return $a;

    };

    foreach ($b as $id => $definition) {

      if (!isset($a[$id]))
        $a[$id] = $definition;

      $a[$id] = $mergeSingleDefinition($a[$id], $definition);

      #if ($definition instanceof Function_) {

      #}

      #var_dump($definition);
      #exit;


    }

    return $a;

    #var_dump($a);
    #var_dump($b);

    #exit;

  }

  static function extractReflectionDefinitions () {

    $definitions = [];

    $internalFunctions = get_defined_functions()['internal'];

    sort($internalFunctions);

    foreach ($internalFunctions as $functionName) {
      echo '3';

      $function_ = new Function_('');

      $functionReflection = new ReflectionFunction($functionName);

      $function_->name = $functionName;

      if ($functionReflection->getExtension()) {

        // @todo: Introduced version.
        #$require_ = $function_->getAttribute('require', []);
        #$require_['php-extension-' . $functionReflection->getExtension()->getName()] =
        #  $functionReflection->getExtension()->getVersion();
        #$function_->setAttribute('require', $require_);

        if ($functionReflection->getExtension()->getName() == 'standard')
          $function_->setAttribute('source-library', 'php-standard');
        else
          $function_->setAttribute('source-library', 'php-extension-' . $functionReflection->getExtension()->getName());

        //$functionReflection->getExtension()

        #var_dump($function_);
        #exit;
      }
      #var_dump($functionReflection->getExtension());
      #exit;

      foreach ($functionReflection->getParameters() as $argumentReflection) {

        $argument = new Param($argumentReflection->getName());

        if (method_exists($argumentReflection, 'hasType') && $argumentReflection->hasType())
          $argument->type = $argumentReflection->getType()->__toString();

        $argument->variadic = $argumentReflection->isVariadic();

        if ($argumentReflection->isDefaultValueAvailable())
          $argument->default = $argumentReflection->getDefaultValue();

        self::patchDefinition($argument);

        $function_->params[] = $argument;
      }

      #var_dump($function_->name);
      #var_dump($function_);

      // Is printable?
      NodeConcept::sourcePrint($function_);

      $definitions['f_' . $function_->name] = $function_;

    }

    $internalClasses = get_declared_classes();
    sort($internalClasses);

    foreach ($internalClasses as $className) {

      $class_ = new Class_('');

      $classReflection = new ReflectionClass($className);

      $class_->name = $className;

      if ($classReflection->getExtension()) {

        // @todo: Introduced version.
        #$require_ = $class_->getAttribute('require', []);
        #$require_['php-extension-' . $classReflection->getExtension()->getName()] =
        #  $classReflection->getExtension()->getVersion();
        #$class_->setAttribute('require', $require_);

        if ($classReflection->getExtension()->getName() == 'standard')
          $class_->setAttribute('source-library', 'php-standard');
        else
          $class_->setAttribute('source-library', 'php-extension-' . $classReflection->getExtension()->getName());

      }

      // Is printable?
      NodeConcept::sourcePrint($class_);

      $definitions['c_' . $class_->name] = $class_;

    }

    return $definitions;

  }

  static function extractDocumentationDefinitions () {

    $definitions = [];

    $document = new \DOMDocument();
    @$document->loadHTML(file_get_contents(self::documentationPath()));

    $documentation = new SimpleXMLElement($document->saveXML());

    foreach ($documentation->xpath('//*[contains(concat(" ", @class, " "), " refsect1 ")]' .
        '/*[contains(concat(" ", @class, " "), " methodsynopsis ")]') as $functionNode) {

      echo '4';

      $function_ = self::parseDocumentationFunction($functionNode);

      if (strpos($function_->name, '::') !== false)
        continue;

      $definitions['f_' . $function_->name] = $function_;

    }

    foreach ($documentation->xpath('//*[contains(concat(" ", @class, " "), " classsynopsis ")]') as $classNode) {

      echo '5';

      $class_ = self::parseDocumentationClass($classNode);

      $definitions['c_' . $class_->name] = $class_;

    }

    return $definitions;

  }

  static function parseDocumentationFunction ($functionNode) {

    $function_ = new Function_('');

    $nameNode = $functionNode->xpath('.//*[contains(concat(" ", @class, " "), " methodname ")]')[0];
    $function_->name = trim(html_entity_decode(strip_tags($nameNode->asXML()), ENT_QUOTES | ENT_HTML5));

    if (count($functionNode->xpath('./*[contains(concat(" ", @class, " "), " type ")]')) > 0) {
      $returnType = trim($functionNode->xpath('./*[contains(concat(" ", @class, " "), " type ")]')[0]->__toString());
      if (!in_array($returnType, ['', 'mixed']))
        $function_->returnType = $returnType;
    }

    if (count($functionNode->xpath('../../..//*[contains(concat(" ", @class, " "), " verinfo ")]')) > 0) {
      $versionInfo = trim(trim($functionNode->xpath('../../..//*[contains(concat(" ", @class, " "), " verinfo ")]')[0]->__toString()), '() ');

      // @todo: Introduced version.
      #$versionInfo = str_replace(',', '|', $versionInfo);
      #$versionInfo = preg_replace_callback('/(?is)PHP ([0-9]+)( \>\= ([0-9\.]+))?/', function ($match) { return '^' . (isset($match[3]) ? $match[3] : $match[1]); }, $versionInfo);

      if (preg_match('/(?is)PECL ([a-z0-9]+)/', $versionInfo, $match)) {
        $function_->setAttribute('source-library', 'php-extension-' . $match[1]);
      } else if (strpos($versionInfo, 'PHP') !== false) {
        $function_->setAttribute('source-library', 'php-standard');
      } else {
        #var_dump($function_);
        #var_dump($versionInfo);
        #exit;
      }

      // 'standard' by default
      // @todo: Rethink.
      #if (!$function_->getAttribute('source-library'))
      #  $function_->setAttribute('source-library', 'php-standard');

      #if (!in_array($returnType, ['', 'mixed']))
      #  $function_->returnType = $returnType;
    }

    #verinfo

    foreach ($functionNode->xpath('.//*[contains(concat(" ", @class, " "), " methodparam ")]') as $argumentNode) {

      if (trim($argumentNode->__toString()) == 'void')
        continue;

      $argument = new Param('');

      $nameNode = $argumentNode->xpath('.//*[contains(concat(" ", @class, " "), " parameter ")]')[0];
      $argumentName = trim(str_replace(['&', '$'], '', html_entity_decode(strip_tags($nameNode->asXML()), ENT_QUOTES | ENT_HTML5)));

      if ($argumentName == '...') {
        $argument->variadic = true;
        $argumentName = '__variadic';
      }

      $argument->name = $argumentName;

      if (count($argumentNode->xpath('.//*[contains(concat(" ", @class, " "), " reference ")]')) > 0)
        $argument->byRef = true;

      if (count($argumentNode->xpath('.//*[contains(concat(" ", @class, " "), " type ")]')) > 0) {
        $type = trim(html_entity_decode(strip_tags($argumentNode->xpath('.//*[contains(concat(" ", @class, " "), " type ")]')[0]->asXML()), ENT_QUOTES | ENT_HTML5));
        if (!in_array($type, ['', 'mixed']))
          $argument->type = $type;
      }

      if (!$argument->variadic) {

        if (count($argumentNode->xpath('preceding-sibling::node()[contains(self::node(), "[")]')) > 0) {

          $defaultValue = new ConstFetch(new NameNode('null'));

          switch ($argument->type) {
            case 'null':
              break;
            case 'bool':
            case 'boolean':
              $defaultValue = new ConstFetch(new NameNode('false'));
              break;
            case 'str':
            case 'string':
              $defaultValue = new String_('');
              break;
            case 'int':
            case 'integer':
              $defaultValue = new LNumber(0);
              break;
          }

          if (count($argumentNode->xpath('.//*[contains(concat(" ", @class, " "), " initializer ")]')) > 0) {

            $initializer = html_entity_decode(strip_tags($argumentNode
              ->xpath('.//*[contains(concat(" ", @class, " "), " initializer ")]')[0]->asXML()),
            ENT_QUOTES | ENT_HTML5);

            if (trim($initializer) == '= NULL')
              $initializer = '= null';

            if (trim($initializer) == '= TRUE')
              $initializer = '= true';

            if (trim($initializer) == '= FALSE')
              $initializer = '= false';

            // Fix for http://www.php.net/manual/en/snmp.setsecurity.php
            if (trim($initializer) == '=')
              $initializer = '';

            // Fix for http://php.net/manual/en/function.fgetcsv.php
            if (trim($initializer) == '= "\\"')
              $initializer = '= "\\\\"';

            // Fix for http://php.net/manual/en/function.trim.php
            if (trim($initializer) == '= " \\t\\n\\r\\0\\x0B"')
              $initializer = '= ""';

            // Fix for http://php.net/manual/en/function.ucwords.php
            if (trim($initializer) == '= " \\t\\r\\n\\f\\v"')
              $initializer = '= ""';

            // Fix for http://www.php.net/manual/en/gearmanclient.addserver.php
            if (trim($initializer) == '= 127.0.0.1')
              $initializer = '= "127.0.0.1"';

            // Fix for http://ie1.php.net/manual/en/gearmanclient.addservers.php
            if (trim($initializer) == '= 127.0.0.1:4730')
              $initializer = '= "127.0.0.1:4730"';

            if ($initializer && strpos(strrev($initializer), strrev('()')) !== 0) {

              try {

              $defaultValue = Code::parse('<?php $x ' . $initializer . ';')[0]->expr;

              } catch (\Exception $e) {
                // Todo: Remove.
                echo $e;
                var_dump($function_);
                echo "\n\n";
                echo '<?php $x ' . $initializer . ';';
                echo "\n\n";
                exit;
              }


            }

          }

          $argument->default = $defaultValue;

        }

      }

      self::patchDefinition($argument);

      $function_->params[] = $argument;

    }

    return $function_;

  }

  static function parseDocumentationClass ($classNode) {

    $class_ = new Class_('');

    if (count($classNode->xpath('../../..//*[contains(concat(" ", @class, " "), " verinfo ")]')) > 0) {
      $versionInfo = trim(trim($classNode->xpath('../../..//*[contains(concat(" ", @class, " "), " verinfo ")]')[0]->__toString()), '() ');

      // @todo: Introduced version.
      #$versionInfo = str_replace(',', '|', $versionInfo);
      #$versionInfo = preg_replace_callback('/(?is)PHP ([0-9]+)( \>\= ([0-9\.]+))?/', function ($match) { return '^' . (isset($match[3]) ? $match[3] : $match[1]); }, $versionInfo);

      if (strpos($versionInfo, 'PHP') !== false) {
        $class_->setAttribute('source-library', 'php-standard');
      } else if (preg_match('/(?is)PECL ([a-z0-9]+)/', $versionInfo, $match)) {
        $class_->setAttribute('source-library', 'php-extension-' . $match[1]);
      } else {
        #var_dump($function_);
        #var_dump($versionInfo);
        #exit;
      }

      // 'standard' by default
      // @todo: Rethink.
      #if (!$function_->getAttribute('source-library'))
      #  $function_->setAttribute('source-library', 'php-standard');

      #if (!in_array($returnType, ['', 'mixed']))
      #  $function_->returnType = $returnType;
    }

    foreach ($classNode->xpath('.//*[contains(concat(" ", @class, " "), " ooclass ")]') as $classHeader) {

      if (count($classHeader->xpath('.//*[contains(concat(" ", @class, " "), " classname ")]')) == 0)
        continue;

      $modifier = '';

      if (count($classHeader->xpath('.//*[contains(concat(" ", @class, " "), " modifier ")]')) > 0)
        $modifier = $classHeader->xpath('.//*[contains(concat(" ", @class, " "), " modifier ")]')[0]->__toString();

      if (in_array($modifier, ['', 'abstract', 'final']))
        $class_->name = $classHeader->xpath('.//*[contains(concat(" ", @class, " "), " classname ")]')[0]->__toString();

      switch ($modifier) {
        case '':
          break;
        case 'abstract':
          $class_->type = $class_->type | Class_::MODIFIER_ABSTRACT;
          break;
        case 'final':
          $class_->type = $class_->type | Class_::MODIFIER_FINAL;
          break;
        case 'extends':
          foreach ($classHeader->xpath('.//*[contains(concat(" ", @class, " "), " classname ")]') as $extendsNode)
            $class_->extends = new NameNode($extendsNode->__toString());
          break;
        case 'implements':
          foreach ($classHeader->xpath('.//*[contains(concat(" ", @class, " "), " classname ")]') as $implementsNode)
            $class_->implements[] = new NameNode($implementsNode->__toString());
          break;
        default:
          echo $classHeader->asXML();
          echo "\n\n";
          exit;
      }

    }

    foreach ($classNode->xpath('.//*[contains(concat(" ", @class, " "), " oointerface ")]') as $classHeader) {
      foreach ($classHeader->xpath('.//*[contains(concat(" ", @class, " "), " interfacename ")][count(./*) = 0]') as $implementsNode)
        $class_->implements[] = new NameNode($implementsNode->__toString());
    }


    foreach ($classNode->xpath('.//*[contains(concat(" ", @class, " "), " methodsynopsis ")]') as $functionNode) {

      if (count($functionNode->xpath('preceding-sibling::node()[contains(self::node(), "/* Inherited methods */")]')) > 0)
        continue;

      $function_ = self::parseDocumentationFunction($functionNode);

      $method_ = new ClassMethod('');
      $method_->name = $function_->name;
      $method_->byRef = $function_->byRef;
      $method_->params = $function_->params;
      $method_->returnType = $function_->returnType;
      $method_->stmts = $function_->stmts;

      self::patchDefinition($method_);

      if (strpos($method_->name, '::') !== false)
        continue;

      $class_->stmts[] = $method_;

    }

    return $class_;

  }

  static function patchDefinition ($definition) {

    // 'php-standard' by default.
    // @todo: Rethink.
    if (!$definition->getAttribute('source-library'))
      $definition->setAttribute('source-library', 'php-standard');

    if (strpos($definition->name, 'mysql_') === 0)
      $definition->setAttribute('source-library', 'php-extension-mysql');

    if (strpos($definition->name, 'mysqli_') === 0)
      $definition->setAttribute('source-library', 'php-extension-mysqli');

    if (strpos($definition->name, 'exif_') === 0 || $definition->name == 'read_exif_data')
      $definition->setAttribute('source-library', 'php-extension-exif');

    if ($definition->getAttribute('source-library') == 'php-extension-Core')
      $definition->setAttribute('source-library', 'php-standard');

    if ($definition->getAttribute('source-library') == 'php-extension-date')
      $definition->setAttribute('source-library', 'php-standard');

    if ($definition->getAttribute('source-library') == 'php-extension-json')
      $definition->setAttribute('source-library', 'php-standard');

    if ($definition->getAttribute('source-library') == 'php-standard' && ($definition instanceof Function_) && !function_exists($definition->name))
      $definition->setAttribute('source-library', '');

    if ($definition->getAttribute('source-library') == 'php-standard' && ($definition instanceof Class_) && !class_exists($definition->name))
      $definition->setAttribute('source-library', '');

    // Fix for http://php.net/manual/en/class.exception.php
    if ($definition instanceof Class_ && $definition->name == 'Exception') {
      $definition->implements[] = new NameNode('Throwable');
    }

    // Fix for http://php.net/manual/en/class.domnodelist.php
    if ($definition instanceof ClassMethod && $definition->name == 'DOMNodelist::item') {
      $definition->name = 'item';
    }

    // Fix for
    // http://php.net/manual/en/imagick.identifyformat.php
    // http://php.net/manual/en/splfileobject.current.php
    if ($definition instanceof ClassMethod && strpos($definition->returnType, '|') !== false) {
      $definition->returnType = null;
    }

    // Fix for
    // http://php.net/manual/en/phar.extractto.php
    // http://php.net/manual/en/phardata.extractto.php
    // http://php.net/manual/en/recursivetreeiterator.construct.php
    if ($definition instanceof Param && strpos($definition->type, '|') !== false) {
      $definition->type = null;
    }

    if ($definition instanceof Function_ && $definition->name == 'preg_match') {
      $comments = $definition->params[2]->getAttribute('comments', []);
      $comments[] = new DocComment('/** @out */');
      $definition->params[2]->setAttribute('comments', $comments);
    }

    if ($definition instanceof Function_ && $definition->name == 'preg_match_all') {
      $comments = $definition->params[2]->getAttribute('comments', []);
      $comments[] = new DocComment('/** @out */');
      $definition->params[2]->setAttribute('comments', $comments);
    }

    if ($definition instanceof Function_ && $definition->name == 'memcache_get') {
      $definition->params = [
        new Param('key', null, 'string'),
        new Param('flags', null, 'int', true),
      ];
    }

  }

  /**
   * Disable XDebug due to performance issues with it.
   * It seems that for data processing heavy code XDebug has
   * massive performance implications.
   */
  static function disableXDebug()
  {
      if (isset($GLOBALS["__xdebug_disable_attempt_made"]))
        return;

      if (!in_array('xdebug', array_map(function ($name) { return strtolower($name); }, get_loaded_extensions(true))))
        return;

      $process = new \Symfony\Component\Process\PhpProcess('<?php
        $GLOBALS["__xdebug_disable_attempt_made"] = true;
        $GLOBALS["argv"] = ' . var_export($GLOBALS['argv'], true) . ';
        require ' . var_export(debug_backtrace()[0]['file'], true) . ';
      ');

      $process->setTimeout(null);

      /**
       * Don't load the default ini file. This is the main reason why we are running
       * a sub-process in the first place - to disable XDebug.
       * It seems that XDebug can't be disabled during runtime, nor can an extension
       * defined in the php.ini be excluded from loading with parameters.
       * So far this seems to be the only way not to load XDebug.
       */
      $process->setCommandLine($process->getCommandLine() . ' -n');

      /**
       * For some reason these two extensions are not statically linked
       * on *nix systems so we need to load the explicitly.
       */
      if (PHP_SHLIB_SUFFIX == 'so')
        $process->setCommandLine($process->getCommandLine() . ' -dextension=tokenizer.so -dextension=json.so');

      $output = fopen('php://stdout', 'w');
      $error = fopen('php://stderr', 'w');

      $process->run(function ($type, $buffer) use ($output, $error) {
        if ($type == \Symfony\Component\Process\Process::OUT)
          fwrite($output, $buffer);
        if ($type == \Symfony\Component\Process\Process::ERR)
          fwrite($error, $buffer);
      });

      /**
       * In case the sub process is successful it runs the original
       * code - without XDebug.
       * In that case continuing to execute would produce a duplicate.
       * If it's not successful the parent process does not exit and
       * continues to fallback with XDebug.
       */
      if ($process->isSuccessful())
        exit;

  }

}
