<?php

namespace luka8088\phlint;

class Result {

  protected $issues = [];

  public $libraryIssues = [];

  function addIssue ($node, $message, $causes = []) {

    if ($node && $node->getAttribute('isTrusted', false))
      return;

    $message = Result::expandMessageMeta($message, $node);

    foreach ($causes as $offset => $causeMessage)
      $message .= "\n  " . 'Cause #' . ($offset + 1) . ': ' .
        str_replace("\n", "\n  ", self::printMessage($causeMessage));

    if (in_array($message, $this->issues))
      return;

    if (in_array($message, $this->libraryIssues))
      return;

    if ($node) {
      $traces = $this->generateTraces($node);

      if (count($traces) > 0) {
        foreach ($traces as $trace) {
          $msg = $message;
          foreach (array_reverse($trace) as $offset => $traceMessage)
            $msg .= "\n  " . 'Trace #' . ($offset + 1) . ': ' . Result::expandMessageMeta($traceMessage['message'], $traceMessage['node']);

          if (in_array($msg, $this->issues))
            return;
          $this->issues[] = $msg;
          context('output')->__invoke($msg . "\n");
        }
        return;
      }
    }

    $this->issues[] = $message;
    context('output')->__invoke($message . "\n");
  }

  function getIssues () {
    return $this->issues;
  }

  public $traceStack = [];

  function pushTrace ($node, $message) {
    $this->traceStack[] = Result::expandMessageMeta($message, $node);
  }

  function popTrace () {
    array_pop($this->traceStack);
  }

  function toString () {
    return implode("\n", array_map(function ($entry) {
          return $entry;
      }, array_slice(array_unique($this->getIssues()), 0, 100))) .
      "\n" . (count(array_unique($this->getIssues())) > 100 ? '...' : '') .
      "\n(" . count(array_unique($this->getIssues())) . ' issue(s) found)';
  }

  static function mergeMessages ($messages) {
    $map = [];
    foreach ($messages as $message) {
      if (!isset($map[$message['message']]))
        $map[$message['message']] = $message;
      if (isset($message['causes']))
        $map[$message['message']]['causes'] = self::mergeMessages(array_merge(
          isset($map[$message['message']]['cause']) ? $map[$message['message']]['cause'] : [],
          $message['causes']
        ));
    }
    return array_values($map);
  }

  static function printMessage ($message) {
    if (is_string($message))
      return $message;
    $printed = $message['message'];
    foreach ($message['causes'] as $offset => $causeMessage)
      $printed .= "\n  " . 'Cause #' . ($offset + 1) . ': ' .
        str_replace("\n", "\n  ", self::printMessage($causeMessage));
    return $printed;
  }

  static function expandMessageMeta ($message, $node) {
    if (!is_object($node))
      return $message;

    if (isset($node->getAttributes()['path'])) {
      $path = $node->getAttributes()['path'];
      if (realpath($path))
        $path = realpath($path);
      return rtrim($message, '.') . ' in *' . $path . ($node->getLine() > 0 ? ':' . $node->getLine() : '') . '*.';
    }

    if ($node->getLine() > 0)
      return rtrim($message, '.') . ' on line ' . $node->getLine() . '.';

    return $message;
  }

  function generateTraces ($node, $participatingInvocationNodes = []) {

    $traces = [];

    $definitionNode = null;
    $contextSymbol = inference\Scope::contextScope($node->getAttribute('scope', []));
    if (isset(context('code')->symbols[$contextSymbol])) {
      foreach (context('code')->symbols[$contextSymbol]['definitionNodes'] as $potentialDefinitionNode)
        NodeTraverser::traverse([$potentialDefinitionNode], [function ($existingNode) use (&$definitionNode, $node, $potentialDefinitionNode) {
          if ($existingNode === $node)
            $definitionNode = $potentialDefinitionNode;
        }]);
    }

    if (!$definitionNode)
      return [];

    $isRecursion = false;
    foreach ($participatingInvocationNodes as $participatingDefinitionNode)
      if ($participatingDefinitionNode === $definitionNode)
        $isRecursion = true;


    if (!$isRecursion) {

      foreach ($definitionNode->getAttribute('symbols', []) as $symbol) {

        $symbol = preg_replace('/(?is)((?<=\.)|\A)s[a-z0-9]*_[^\.]*(\.|\z)/', '', $symbol);

        if (isset(context('code')->symbols[$symbol]['linkedNodes']))
          foreach (context('code')->symbols[$symbol]['linkedNodes'] as $linkedNode) {

            // @todo: Rewrite
            if ($linkedNode->getAttribute('invocationTypes', []) != $definitionNode->getAttribute('types', []))
              continue;

            $linkedNodeTraces = $this->generateTraces($linkedNode, array_merge($participatingInvocationNodes, [$definitionNode]));

            $message = ucfirst(NodeConcept::referencePrint($definitionNode)) .
              ' specialized for the ' . NodeConcept::referencePrint($linkedNode) . '.' . ($isRecursion ? 'recursion detected!' : '');

            foreach (count($linkedNodeTraces) > 0 ? $linkedNodeTraces : [0 => ''] as $traceIndex => $_) {
              if (!isset($linkedNodeTraces[$traceIndex]))
                $linkedNodeTraces[$traceIndex] = [];
              $linkedNodeTraces[$traceIndex][] = ['message' => $message, 'node' => $linkedNode];
            }

            foreach ($linkedNodeTraces as $linkedNodeTrace)
              $traces[] = $linkedNodeTrace;

          }

      }

    }

    // @todo: Remove or rewrite.
    $traces = array_slice($traces, 0, 5);

    return $traces;

  }

}
