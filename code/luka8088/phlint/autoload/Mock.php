<?php

namespace luka8088\phlint\autoload;

class Mock {

  function __construct ($data) {
    $this->data = $data;
  }

  function __invoke ($class) {
    if (isset($this->data[$class]))
      context('code')->load([[
        'path' => 'mock:' . $class,
        'source' => $this->data[$class],
        'isLibrary' => true,
      ]]);
  }

}
