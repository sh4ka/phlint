<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Result;
use \PhpParser\Comment;
use \PhpParser\Node;

/**
 * @see /documentation/attribute/index.md
 */
class Attribute {

  function getIdentifier () {
    return 'attribute';
  }

  function visitNode ($node) {

    $unwrap = function ($comment) {
      if (strpos(ltrim($comment), '//') === 0)
        return substr(ltrim($comment), 2);
      if (strpos(ltrim($comment), '/**') === 0)
        return preg_replace('/(?is)(\A\/\*\*|(?<=\n)[ \t]*\*(?!\/\z)|\*\/\z)/', '', $comment);
      return $comment;
    };

    $toString = function ($comment) {
      if ($comment instanceof Comment) {
        return $comment->getText();
      }
      return $comment;
    };

    $comments = array_map($unwrap, array_map($toString, $node->getAttribute('comments', [])));

    $attributes = [];

    foreach ($comments as $comment) {
      if (preg_match('/(?is)(?<![a-z0-9_\-])@([a-z0-9_\-]*)/', $comment, $match)) {
        $attribute = $match[1];
        if ($attribute == 'var') {
          $variableRegex = '[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*';
          $typeRegex = '[a-zA-Z_\x7f-\xff\\\\][a-zA-Z0-9_\x7f-\xff\\\\\[\]]*';
          preg_match(
            '/(?is)(?<![a-z0-9_\-])@([a-z0-9_\-]*)([ \t]+(' . $typeRegex . ')([ \t]+(\$[a-z0-9_\-]*))?)?/',
            $comment,
            $match
          );
          $attribute = [
            'name' => $match[1],
            'arguments' => array_values(array_filter([
              isset($match[3]) ? $match[3] : '',
              isset($match[5]) ? $match[5] : '',
            ])),
          ];
        }
        $attributes[] = $attribute;
      }
    }

    $node->setAttribute('attributes', array_merge($node->getAttribute('attributes', []), $attributes));

  }

  /**
   * Get node analysis-time known attributes.
   * This function is meant to be used by rules and other inferences
   * as it implements some lightweight deduction logic.
   *
   * @param Node $node
   * @return mixed[]
   */
  static function get ($node) {

    $attributes = [];

    foreach ($node->getAttribute('attributes', []) as $attribute)
      $attributes[] = $attribute;

    return array_values($attributes);

  }

}
