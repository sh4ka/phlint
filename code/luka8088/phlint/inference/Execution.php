<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\inference\Value;
use \luka8088\phlint\NodeTraverser;
use \PhpParser\Node;

class Execution {

  function getIdentifier () {
    return 'execution';
  }

  function getPass () {
    return 50;
  }

  function getDependencies () {
    return [
      'value',
    ];
  }

  function visitNode ($node) {

    if ($node instanceof Node\Stmt\If_)
      Execution::inferIsReachable($node->cond, $node->stmts);

  }

  static function inferIsReachable ($condition, $statements) {

    $values = Value::get($condition);
    $isReachable = true;

    if (count($values) == 1 && $values[0]['type'] == 't_bool' && $values[0]['value'] == false)
      $isReachable = false;

    NodeTraverser::traverse($statements, [function ($node) use ($isReachable) {
      $node->setAttribute('isReachable', $isReachable);
    }]);

  }

}
