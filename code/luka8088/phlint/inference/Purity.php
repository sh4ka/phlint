<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Result;
use \PhpParser\Node;

/**
 * Purity inference.
 *
 * @see /documentation/glossary/purity.md
 */
class Purity {

  function getIdentifier () {
    return 'purity';
  }

  function getDependencies () {
    return [
      'execution',
      'isolation',
      'symbolLink',
    ];
  }

  function getPass () {
    return 60;
  }

  function visitNode ($node) {

    if (!$node->getAttribute('isReachable', true))
      return;

    #if ($node instanceof Node\Expr\FuncCall)
    #  foreach (Symbol::get($node) as $symbol)
    #    if (isset(context('code')->symbols[$symbol]))
    #      foreach (context('code')->symbols[$symbol]['definitionNodes'] as $linkedNode)
    #        foreach ($linkedNode->getAttribute('purityBreaches', []) as $purityBreach)
    #          $this->addPurityBreach($node, $purityBreach);

  }

  function addPurityBreach ($node, $message, $causes = []) {
    $context = context('code')->scopes[$node->getAttribute('scope')]['context'];
    if (!isset($context['node']))
      return;
    $purityBreaches = $context['node']->getAttribute('purityBreaches', []);
    $purityBreaches[] = [
      'message' => is_string($message) ? Result::expandMessageMeta($message, $node) : $message['message'],
      'causes' => array_merge(is_string($message) ? [] : $message['causes'], $causes),
    ];
    $context['node']->setAttribute('purityBreaches', Result::mergeMessages($purityBreaches));
  }

}
