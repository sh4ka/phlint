<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeTraverser;

class Simulation {

  function getIdentifier () {
    return 'simulation';
  }

  function enterNode ($node) {

    if ($node->getAttribute('isLibrary', false))
      return;

    if (NodeConcept::isExecutionContextNode($node)) {

      $simulationIdCounter = 0;

      foreach (NodeConcept::alternativeExecutionBranchesIterator($node) as $executionBranchNode) {

        $simulationIdCounter += 1;

        NodeTraverser::traverse($executionBranchNode, [function ($subNode) use ($simulationIdCounter) {

          $simulations = $subNode->getAttribute('simulations', []);
          $simulations[$simulationIdCounter] = [];
          $subNode->setAttribute('simulations', $simulations);

        }]);

      }

    }

    if (count($node->getAttribute('simulations', [])) == 0)
      $node->setAttribute('simulations', [0 => []]);


  }

  /**
   * Is an execution flow set $subset a subset of $set.
   *
   * This means that:
   *   - Something that happens in $subset will also happen in $set.
   *   - Something provided in $set will be always available in $subset.
   */
  static function isSubset ($set, $subset) {
    return count(array_diff($subset, $set)) == 0;
  }

}
