<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\Code;
use \luka8088\phlint\inference;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeTraverser;
use \PhpParser\Node;

class TemplateSpecialization {

  function getIdentifier () {
    return 'templateSpecialization';
  }

  function getDependencies () {
    return [
      'symbol',
      'type',
      'value',
    ];
  }

  function getPass () {
    return 50;
  }

  function visitNode ($node) {

    if (NodeConcept::isInvocationNode($node)) {

      $specializationSymbols = [];

      foreach (Symbol::get($node) as $symbol) {

        if (!isset(context('code')->symbols[$symbol]))
          continue;

        // @todo: Implement.
        if ($symbol == 'c_closure' || $symbol == 'c_closure()')
          continue;

        foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode) {

          if ($definitionNode->getAttribute('isSpecializationTemp', false))
            continue;

          #$f = new Node\Stmt\Function_('');
          #$f->setAttribute('isSpecializationTemp', true);
          #context('code')->symbols[$symbol]['definitionNodes'][] = $f;

          #var_dump(count(context('code')->symbols[$symbol]['definitionNodes']));

          $specializationSymbol = $symbol . TemplateSpecialization::specializationSignature($node) . $definitionNode->getAttribute('startLine');

          if (!isset(context('code')->symbols[$specializationSymbol])) {

            context('code')->symbols[$specializationSymbol] = [
              'id' => $specializationSymbol,
              'phpId' => isset(context('code')->symbols[$symbol]) ? context('code')->symbols[$symbol]['phpId'] : $specializationSymbol,
              'aliasOf' => '',
              'definitionNodes' => [],
            ];

            /**
             * Specializing functions without a body makes no difference as they
             * behave the same - they don't do anything.
             * This is only for optimization purposes.
             */
            if (false && count($definitionNode->stmts) == 0)
              $specializationNode = $definitionNode;
            else {

              if (!NodeConcept::isExecutionContextNode($definitionNode))
                var_dump($definitionNode, $node, $specializationSymbol);

              assert(NodeConcept::isExecutionContextNode($definitionNode));

              $specializationNode = NodeConcept::deepClone($definitionNode);

              $isSpecialization = false;

              foreach ($specializationNode->params as $parameter)
                if (count(Type::get($parameter)) == 0 && count(Value::get($parameter)) == 0)
                  $isSpecialization = true;

              if (count($specializationNode->getAttribute('returnTypes', [])) == 0 && count($specializationNode->getAttribute('returnValues', [])) == 0)
                $isSpecialization = true;

              if ($isSpecialization)
                NodeTraverser::traverse($specializationNode, [function ($specializationNode) {
                  $specializationNode->setAttribute('isSpecialization', true);
                }]);

              if (NodeConcept::isExecutionContextNode($specializationNode)) {

                  foreach ($node->args as $offset => $argument) {
                    if (isset($specializationNode->params[$offset])) {
                      $specializationNode->params[$offset]->setAttribute('types', Type::get($argument));
                      if (count(Value::get($argument)) > 0)
                        $specializationNode->params[$offset]->setAttribute('values', Value::get($argument));
                      else
                        $specializationNode->params[$offset]->setAttribute('values', [['type' => 't_mixed', 'value' => '']]);
                    }

                  }

              }

              Code::infer([$specializationNode]);

              $specializationNode->setAttribute('isSpecializationTemp', true);

            }

            if ($specializationNode && $specializationNode !== $definitionNode)
              context('code')->symbols[$symbol]['definitionNodes'][] = $specializationNode;

            if ($specializationNode) {
              #context('code')->symbols[$specializationSymbol]['nodes'][] = $specializationNode;

              $isNodeAttached = false;
              foreach (context('code')->symbols[$specializationSymbol]['definitionNodes'] as $existingDefinitionNode)
                if ($existingDefinitionNode === $specializationNode)
                  $isNodeAttached = true;
              if (!$isNodeAttached)
                context('code')->symbols[$specializationSymbol]['definitionNodes'][] = $specializationNode;

            }

          }

          foreach (context('code')->symbols[$specializationSymbol]['definitionNodes'] as $specializationNode) {

            $node->setAttribute('invocationTypes', [NodeConcept::displayPrint($specializationNode)]);
            $specializationNode->setAttribute('types', [NodeConcept::displayPrint($specializationNode)]);

            foreach ($specializationNode->getAttribute('symbols', []) as $specializationSymbol) {

              // @todo: Rewrite
              if (!isset(context('code')->symbols[$specializationSymbol]))
                context('code')->symbols[$specializationSymbol] = [
                  'id' => $specializationSymbol,
                  'phpId' => isset(context('code')->symbols[$symbol]) ? context('code')->symbols[$symbol]['phpId'] : $specializationSymbol,
                  'aliasOf' => '',
                  'definitionNodes' => [],
                ];

              $isNodeAttached = false;
              foreach (context('code')->symbols[$specializationSymbol]['definitionNodes'] as $existingDefinitionNode)
                if ($existingDefinitionNode === $specializationNode)
                  $isNodeAttached = true;
              if (!$isNodeAttached)
                context('code')->symbols[$specializationSymbol]['definitionNodes'][] = $specializationNode;

              #context('code')->symbols[$specializationSymbol]['definitionNodes'][] = $specializationNode;

              $specializationSymbols[] = $specializationSymbol;
            }

          }

        }

      }

      #$node->setAttribute('symbols', array_unique($specializationSymbols));

    }

  }

  static function specializationSignature ($node) {

    if (NodeConcept::isInvocationNode($node))
      return '(' . implode(', ', array_map(function ($argument) {
        return implode('|', array_merge(Type::get($argument), array_map(function ($value) {
          return $value['type'] . ':' . $value['value'];
        }, Value::get($argument))));
      }, $node->args)) . ')';

    return '';

  }

}
