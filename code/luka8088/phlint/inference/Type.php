<?php

namespace luka8088\phlint\inference;

use \ArrayObject;
use \luka8088\phlint\inference\Attribute;
use \luka8088\phlint\inference\Scope;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\phpLanguage;
use \PhpParser\Comment;
use \PhpParser\Node;

class Type {

  function getIdentifier () {
    return 'type';
  }

  function getPass () {
    return [30, 40, 60];
  }

  function getDependencies () {
    return [
      'attribute',
      'scope',
      'symbol',
      'typeLiteral',
    ];
  }

  public $symbolTypes = [];

  function resetState () {
    $this->symbolTypes = [];
  }

  function beforeTraverse () {
    $this->resetState();

    context('code')->symbols['t_mixed'] = [
      'id' => 't_mixed',
      'phpId' => 'mixed',
      'aliasOf' => '',
      'definitionNodes' => [],
    ];

    foreach (phpLanguage\Fixture::$implicitTypeConversions as $type1 => $types2) {
      if (!isset(context('code')->symbols[Symbol::fullyQualifiedIdentifier($type1, 'type')]))
        context('code')->symbols[Symbol::fullyQualifiedIdentifier($type1, 'type')] = [
          'id' => Symbol::fullyQualifiedIdentifier($type1, 'type'),
          'phpId' => $type1,
          'aliasOf' => '',
          'definitionNodes' => [],
        ];
      foreach ($types2 as $type2)
        if (!isset(context('code')->symbols[Symbol::fullyQualifiedIdentifier($type2, 'type')]))
          context('code')->symbols[Symbol::fullyQualifiedIdentifier($type2, 'type')] = [
            'id' => Symbol::fullyQualifiedIdentifier($type2, 'type'),
            'phpId' => $type2,
            'aliasOf' => '',
            'definitionNodes' => [],
          ];
    }

  }

  function visitNode ($node) {

    $this->inferConditionalGuarantees($node);

    $this->inferSymbolTypes($node);

    $this->inferTypes($node);

    self::interTypesDisplay($node);

  }

  function inferConditionalGuarantees ($node) {

    foreach (phpLanguage\ConditionalGuarantee::evaluate($node) as $guarantee) {

      if (count($guarantee['includesTypes']) > 0) {

        $types = [];

        foreach ($guarantee['includesTypes'] as $includesTypesNode)
          foreach (Symbol::get($includesTypesNode) as $type)
            $types[] = $type;

        foreach (Scope::get($guarantee['scope']) as $scope)
          foreach (Symbol::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            $this->symbolTypes[$scopedSymbol] = $types;
          }

      }

    }

  }

  function inferSymbolTypes ($node) {

    if ($node instanceof Node\Expr\Assign)
      $this->registerSymbolsTypes($node->var, Type::get($node->expr));

    if ($node instanceof Node\Expr\Assign) {

      if ($node->var instanceof Node\Expr\ArrayDimFetch) {

        if (!$node->var->dim || count($node->var->var->getAttribute('isInitialization', [])) > 0)
        $this->registerSymbolsTypes($node->var->var, array_filter(array_map(function ($type) { if ($type == 't_undefined') return ''; return $type . '[]'; }, Type::get($node->expr))));

      }

      if ($node->var instanceof Node\Expr\Variable) {
        foreach ($node->var->getAttribute('attributes', []) as $attribute) {
          if (isset($attribute['name']) && $attribute['name'] == 'var' && isset($attribute['arguments'][0])) {
            foreach (Scope::get($node->var) as $scope) {
              $typeSymbol = Symbol::lookupSinglePhpId($attribute['arguments'][0], $scope);
              if ($typeSymbol == 't_string')
                $typeSymbol = 't_autoString';
              if ($typeSymbol == 't_string[]')
                $typeSymbol = 't_autoString[]';
              if ($typeSymbol == 't_int')
                $typeSymbol = 't_autoInteger';
              if ($typeSymbol == 't_int[]')
                $typeSymbol = 't_autoInteger[]';
              if ($typeSymbol)
                $this->registerSymbolsTypes($node->var, [$typeSymbol]);
            }
          }
        }
      }

    }

    if ($node instanceof Node\Param)
      $this->registerSymbolsTypes($node, Type::get($node));

    if ($node instanceof Node\Param)
      foreach (Attribute::get($node) as $attribute) {
        if (isset($attribute['name']) && $attribute['name'] == 'var' && isset($attribute['arguments'][0])) {
          foreach (Scope::get($node) as $scope) {
            $typeSymbol = Symbol::lookupSinglePhpId($attribute['arguments'][0], $scope);
            if ($typeSymbol == 't_string')
              $typeSymbol = 't_autoString';
            if ($typeSymbol == 't_string[]')
              $typeSymbol = 't_autoString[]';
            if ($typeSymbol == 't_int')
              $typeSymbol = 't_autoInteger';
            if ($typeSymbol == 't_int[]')
              $typeSymbol = 't_autoInteger[]';
            if ($typeSymbol)
              $this->registerSymbolsTypes($node, [$typeSymbol]);
          }
        }
      }

    if ($node instanceof Node\Stmt\Foreach_) {
      if ($node->keyVar)
        $this->registerSymbolsTypes($node->keyVar, Type::get($node->keyVar));
      if ($node->expr && $node->valueVar) {
        $valueTypes = [];
        foreach (Type::get($node->expr) as $type)
          if (substr($type, -2) == '[]')
            $valueTypes[] = substr($type, 0, -2);
        $this->registerSymbolsTypes($node->valueVar, array_unique(array_merge(Type::get($node->valueVar), $valueTypes)));
      }
    }

  }

  function inferTypes ($node) {

    if (NodeConcept::isRhsSymbolNode($node))
      $node->setAttribute('types', $this->lookup($node));

    if (count(Symbol::get($node)) == 0)
      return;

    if (NodeConcept::isExecutionContextNode($node)) {
      $returnTypes = [];
      if ($node->returnType && Symbol::identifier($node->returnType, 'auto'))
        $returnTypes[] = Symbol::identifier($node->returnType, 'auto');
      foreach ($this->inferReturnTypes($node) as $returnType)
        $returnTypes[] = $returnType;
      $node->setAttribute('returnTypes', array_unique($returnTypes));
    }

  }

  function registerSymbolsTypes ($symbols, $types) {

    if ($symbols instanceof Node)
      $symbols = Symbol::get($symbols);

    foreach ($symbols as $symbol) {

      if (!$symbol)
        continue;

      $this->symbolTypes[$symbol] = [];

      foreach (Symbol::visibleScopes($symbol) as $scopeSymbol) {
        if (!isset($this->symbolTypes[$scopeSymbol]))
          $this->symbolTypes[$scopeSymbol] = [];
        $this->symbolTypes[$scopeSymbol] = array_unique(array_merge($this->symbolTypes[$scopeSymbol], $types));
      }

    }

  }

  function lookup ($symbols) {

    if (is_object($symbols) && NodeConcept::isInvocationNode($symbols)) {

      $types = [];

      foreach (Symbol::get($symbols) as $symbol)
        if (isset(context('code')->symbols[$symbol]))
          foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode)
            if ($symbols->getAttribute('invocationTypes', []) == $definitionNode->getAttribute('types', []))
              if (NodeConcept::isExecutionContextNode($definitionNode)) {
                $types = [];
                if ($definitionNode->returnType && Symbol::identifier($definitionNode->returnType, 'auto'))
                  $types[] = Symbol::identifier($definitionNode->returnType, 'auto');
                foreach ($definitionNode->getAttribute('returnTypes', []) as $type)
                  $types[] = $type;
                #$this->registerSymbolsTypes($symbols, $types);
              }

      return array_unique($types);

    }

    if ($symbols instanceof Node)
      return $this->lookup(Symbol::get($symbols));

    $types = [];

    foreach ($symbols as $symbol) {
      $unqualifiedSymbol = Symbol::unqualified($symbol);
      foreach (Scope::visibleScopes(Scope::symbolScope($symbol)) as $visibleScope) {
        $lookupSymbol = Symbol::concat($visibleScope, $unqualifiedSymbol);
        if (isset($this->symbolTypes[$lookupSymbol])) {
          foreach ($this->symbolTypes[$lookupSymbol] as $type)
            $types[] = $type;
          break;
        }
      }
    }

    return array_unique($types);

  }

  function inferReturnTypes ($node) {

    $returnTypes = [];

    NodeTraverser::traverse($node, [function ($node) use (&$returnTypes) {
      if ($node instanceof Node\Stmt\Return_)
        $returnTypes = array_unique(array_merge($returnTypes, Type::get($node->expr)));
    }]);

    assert(count($returnTypes) == count(array_filter($returnTypes)));

    return $returnTypes;

  }

  static function literal ($node) {

    if ($node instanceof Node\Expr\Array_) {
      $types = [];
      foreach ($node->items as $itemNode) {
        $literalType = Type::literal($itemNode);
        if (!in_array($literalType, $types))
          $types[] = $literalType;
      }
      $common = Type::common($types);
      return $common ? $common . '[]' : '';
    }

    if ($node instanceof Node\Expr\ArrayItem)
      return Type::literal($node->value);

    if ($node instanceof Node\Expr\ConstFetch) {
      if (strtolower($node->name->toString()) == 'null')
        return '';
      if (strtolower($node->name->toString()) == 'true')
        return '';
      if (strtolower($node->name->toString()) == 'false')
        return '';
    }

    if ($node instanceof Node\Scalar\DNumber)
      return 't_float';

    if ($node instanceof Node\Scalar\LNumber)
      return 't_int';

    if ($node instanceof Node\Scalar\MagicConst) {
      if ($node instanceof Node\Scalar\MagicConst\Line)
        return 't_int';
      return 't_string';
    }

    if ($node instanceof Node\Scalar\String_) {
      if (((string) ((int) $node->value)) === (string) $node->value)
        return 't_autoInteger';
      if (is_numeric($node->value))
        return 't_autoFloat';
      return 't_string';
    }

    return '';

  }

  static $_implicitTypeConversionsMap = [];

  static function implicitTypeConversionsMap () {

    if (count(Type::$_implicitTypeConversionsMap) == 0) {

      $populate = function ($type, $implicitlyConvertibleTypes) use (&$populate) {
        foreach ($implicitlyConvertibleTypes as $implicitlyConvertibleType) {
          Type::$_implicitTypeConversionsMap[Symbol::identifier($type, 'auto')] = array_unique(array_merge(
            Type::$_implicitTypeConversionsMap[Symbol::identifier($type, 'auto')],
            [Symbol::identifier($implicitlyConvertibleType, 'auto')]
          ));
          if (isset(phpLanguage\Fixture::$implicitTypeConversions[$implicitlyConvertibleType]))
            $populate($type, phpLanguage\Fixture::$implicitTypeConversions[$implicitlyConvertibleType]);
        }
      };

      foreach (phpLanguage\Fixture::$implicitTypeConversions as $type => $implicitlyConvertibleTypes) {
        Type::$_implicitTypeConversionsMap[Symbol::identifier($type, 'auto')] = [
          Symbol::identifier($type, 'auto'),
        ];
        $populate($type, $implicitlyConvertibleTypes);
      }

    }

    return Type::$_implicitTypeConversionsMap;

  }

  /**
   * Is $type1 implicitly convertible to $type2
   */
  static function isImplicitlyConvertible ($type1, $type2) {

    if ($type1 == $type2)
      return true;

    if ($type1 == '' || $type2 == '')
      return false;

    if (isset(Type::implicitTypeConversionsMap()[$type1]))
      foreach (Type::implicitTypeConversionsMap()[$type1] as $type1ImplicitlyConvertible)
        if ($type1ImplicitlyConvertible == $type2)
          return true;

    return false;

  }

  static function common ($types) {

    $types = array_unique($types);

    if (count($types) == 0)
      return '';

    if (count($types) > count(array_filter($types)))
      return '';

    static $cachex = [];

    if (isset($cachex[serialize($types)]))
      return $cachex[serialize($types)];

    foreach ($types as $t)
      if ($t)
        assert(substr($t, 1, 1) == '_', $t);

    $isCompatibleCandidate = function ($typeCandidate) use ($types) {
      foreach ($types as $type) {
        if (!Type::isImplicitlyConvertible($type, $typeCandidate))
          return false;
      }
      return true;
    };

    $compatibleCandidates = [];

    foreach ($types as $type) {
      if ($isCompatibleCandidate($type))
        $compatibleCandidates[] = $type;
      if (isset(Type::implicitTypeConversionsMap()[$type]))
        foreach (Type::implicitTypeConversionsMap()[$type] as $typeCandidate)
          if ($isCompatibleCandidate($typeCandidate))
            $compatibleCandidates[] = $typeCandidate;
    }

    $compatibleCandidates = array_unique($compatibleCandidates);

    $tooGeneralcandidates = [];

    foreach ($compatibleCandidates as $compatibleCandidate1)
      foreach ($compatibleCandidates as $compatibleCandidate2)
        if ($compatibleCandidate1 != $compatibleCandidate2)
          if (Type::isImplicitlyConvertible($compatibleCandidate1, $compatibleCandidate2))
            $tooGeneralcandidates[] = $compatibleCandidate2;

    $tooGeneralcandidates = array_unique($tooGeneralcandidates);

    $successfulCandidates = array_values(array_diff($compatibleCandidates, $tooGeneralcandidates));

    if (count($successfulCandidates) > 0) {
      $cachex[serialize($types)] = $successfulCandidates[0];
      return $successfulCandidates[0];
    }

    $cachex[serialize($types)] = '';
    return '';

  }

  /** @internal @test */
  static function unittest_common () {
    assert(self::common(['t_X', 't_X', 't_X']) == 't_X');
    assert(self::common(['t_X', 't_Y', 't_Y', 't_X']) == '');
    assert(self::common(['t_int', 't_float']) == 't_float');
    assert(self::common(['t_autoInteger', 't_autoFloat']) == 't_autoFloat');
    assert(self::common(['t_int', 't_int']) == 't_int');
    assert(self::common(['t_int', 't_string']) == 't_autoString');
    assert(self::common(['t_int', 't_float', 't_autoInteger', 't_int']) == 't_autoFloat');
  }

  static function interTypesDisplay ($node) {

    $typesDisplay = [];

    foreach (Type::get($node) as $type) {

      if (isset(context('code')->symbols[$type]) && context('code')->symbols[$type]['phpId']) {
        $typesDisplay[] = context('code')->symbols[$type]['phpId'];
        continue;
      }

      $typesDisplay[] = str_replace('t_', '', $type);

    }

    if (count($typesDisplay) > 0)
      $node->setAttribute('typesDisplay', array_unique($typesDisplay));

  }

  /**
   * Get node types.
   * This function is meant to be used by rules and other inferences
   * as it implements some lightweight deduction logic.
   */
  static function get ($node) {

    $types = [];

    if ($node instanceof Node)
      foreach ($node->getAttribute('types', []) as $type)
        $types[] = $type;

    if ($node instanceof Node\Arg)
      foreach (Type::get($node->value) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\Array_) {
      $itemTypes = [];
      foreach ($node->items as $itemNode) {
        $itemNodeTypes = Type::get($itemNode);
        if (count($itemNodeTypes) == 0)
          $itemTypes[] = '';
        foreach ($itemNodeTypes as $itemNodeType)
          $itemTypes[] = $itemNodeType;
      }
      $common = Type::common(array_unique($itemTypes));
      if ($common)
        $types[] = $common . '[]';
    }

    if ($node instanceof Node\Expr\ArrayDimFetch)
      foreach (Type::get($node->var) as $arrayType)
        $types[] = substr($arrayType, -2) == '[]' ? substr($arrayType, 0, -2) : 't_mixed';

    if ($node instanceof Node\Expr\ArrayItem)
      foreach (Type::get($node->value) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\BinaryOp\BooleanAnd)
      $types[] = 't_bool';

    if ($node instanceof Node\Expr\Closure)
      foreach (Symbol::get($node) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\New_)
      foreach (Type::get($node->class) as $type)
        $types[] = $type;

    if ($node instanceof Node\Expr\Ternary)
      foreach (array_merge(Type::get($node->if), Type::get($node->else)) as $type)
        $types[] = $type;

    if ($node instanceof Node\Name)
      foreach (Symbol::get($node) as $type)
        $types[] = $type;

    if (($node instanceof Node\Param) && ($node->type instanceof Node\Name))
      foreach (Type::get($node->type) as $type)
        $types[] = $type;

    $literalType = Type::literal($node);

    if ($literalType)
      $types[] = $literalType;

    return array_unique($types);

  }

  static function isCompatibleInvocation ($invocationType, $declarationType) {

    if (($invocationType instanceof Node\Expr\FuncCall) && ($declarationType instanceof Node\Stmt\Function_)) {

      foreach ($declarationType->params as $index => $parameterNode) {

        if (!$parameterNode->default && !isset($invocationType->args[$index]))
          return false;

        if (!isset($invocationType->args[$index]))
          continue;

        foreach (Type::get($invocationType->args[$index]) as $argumentType)
          foreach (Type::get($parameterNode) as $parameterType)
            if (!Type::isImplicitlyConvertible($argumentType, $parameterType))
              return false;

        #var_dump(Type::get($parameter));
        #var_dump(Type::get($invocationType->args[$index]));
        #exit;
      }

      return true;
    }

    #var_dump('x');
    #exit;

    return false;

  }

}
