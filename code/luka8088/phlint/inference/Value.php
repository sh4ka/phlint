<?php

namespace luka8088\phlint\inference;

use \luka8088\phlint\inference\Scope;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\NodeTraverser;
use \luka8088\phlint\phpLanguage;
use \PhpParser\Node;

class Value {

  function getIdentifier () {
    return 'value';
  }

  function getPass () {
    return [30, 40];
  }

  public $symbolValues = [];

  function resetState () {
    $this->symbolValues = [];
  }

  function beforeTraverse () {
    $this->resetState();
  }

  function visitNode ($node) {

    if (!($node instanceof Node\Param) && count($node->getAttribute('values', [])) > 0)
      $node->setAttribute('values', []);

    $this->inferConditionalGuarantees($node);

    $this->inferSymbolValues($node);

    $this->inferValues($node);

    $this->evaluateExpression($node);

    $this->evaluateArrayLiteral($node);

  }

  function leaveNode ($node) {

    $this->inferConditionalGuaranteesBarrier($node);

  }

  function inferConditionalGuarantees ($node) {

    foreach (phpLanguage\ConditionalGuarantee::evaluate($node) as $guarantee) {

      foreach ($guarantee['excludesValues'] as $value) {

        foreach (Scope::get($guarantee['scope']) as $scope)
          foreach (Symbol::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            if (!isset($this->symbolValues[$scopedSymbol]))
              $this->symbolValues[$scopedSymbol] = [];
            unset($this->symbolValues[$scopedSymbol][Value::valueKey($value)]);
          }

      }

      if (count($guarantee['includesTypes']) > 0) {

        foreach (Scope::get($guarantee['scope']) as $scope)
          foreach (Symbol::get($guarantee['node']) as $symbol) {
            $scopedSymbol = Symbol::concat($scope, Symbol::unqualified($symbol));
            $this->symbolValues[$scopedSymbol] = [];
          }

      }

    }

  }

  function inferConditionalGuaranteesBarrier ($node) {

    if ($node instanceof Node\Stmt\If_) {

      $hasBarrier = false;

      foreach ($node->stmts as $statementNode)
        if (NodeConcept::isLoopScopeBarrier($statementNode))
          $hasBarrier = true;

      if ($hasBarrier) {

        $guarantees = phpLanguage\ConditionalGuarantee::evaluateCondition(new Node\Expr\BooleanNot($node->cond));

        foreach ($guarantees as $guarantee) {

          foreach ($guarantee['excludesValues'] as $value) {
            foreach (Symbol::get($guarantee['node']) as $symbol) {
              #var_dump($symbol);
              #if (!isset($this->symbolValues[$symbol]))
              #  $this->symbolValues[$symbol] = [];
              #var_dump('guarantee: ' . $symbol);


              $scope = Scope::symbolScope($symbol);

              while (true) {

                $symbolx = $scope . '.' . Symbol::unqualified($symbol);

                #var_dump('guarantee: ' . $symbolx);

                assert(substr($symbolx, 0, 1) != '.');
                assert(substr($symbolx, -1) != '.');

                if (!isset($this->symbolValues[$symbolx]))
                  $this->symbolValues[$symbolx] = [];

                unset($this->symbolValues[$symbolx][Value::valueKey($value)]);

                #if ($symbolx && isset($this->symbolTypes[$symbolx])) {
                #  unset($this->symbolValues[$symbolx][Value::valueKey($value)]);
                #  break;
                #}

                if (NodeConcept::isLoop(context('code')->scopes[$scope]['node']))
                  break;

                if (Symbol::isContext($scope))
                  break;

                $scope = Scope::parent($scope);

                if (!$scope)
                  break;

              }

            }
          }

        }

      }

    }

  }

  function inferSymbolValues ($node) {

    if ($node instanceof Node\Expr\Assign)
      $this->registerSymbolsValues($node->var, Value::get($node->expr));

    if ($node instanceof Node\Param)
      foreach (Symbol::get($node) as $symbol)
        $this->registerSymbolsValues($node, Value::get($node));

    /**
     * Constants need to be registered globally as they are available outside
     * of the registered scope.
     */
    if ($node instanceof Node\Stmt\Const_)
      foreach ($node->consts as $constantNode)
        foreach (Symbol::get($constantNode) as $constantSymbol)
          if (isset(context('code')->symbols[$constantSymbol]))
            foreach (Value::literal($constantNode) as $value)
              context('code')->symbols[$constantSymbol]['values'][Value::valueKey($value)] = $value;

    if (($node instanceof Node\Expr\FuncCall) && Symbol::get($node) == ['f_define'] && count($node->args) >= 2)
      foreach (Value::literal($node->args[0]) as $constantName) {
        #if ($constantName['type'] != 't_string')
        #  continue;
        $symbol = Symbol::fullyQualifiedIdentifier('\\' . ltrim($constantName['value'], '\\'), 'constant');
        if (!isset(context('code')->symbols[$symbol]))
          continue;
        foreach (Value::literal($node->args[1]) as $value)
          context('code')->symbols[$symbol]['values'][Value::valueKey($value)] = $value;
      }

    if ($node instanceof Node\Param)
      $this->registerSymbolsValues($node, Value::get($node));

  }

  function inferValues ($node) {
    if (NodeConcept::isRhsSymbolNode($node))
      $node->setAttribute('values', $this->lookup($node));

    if (NodeConcept::isExecutionContextNode($node)) {
      $returnValues = [];
      NodeTraverser::traverse($node, [function ($node) use (&$returnValues) {
        if ($node instanceof Node\Stmt\Return_)
          foreach (Value::get($node->expr) as $value)
            $returnValues[Value::valueKey($value)] = $value;
      }]);
      $node->setAttribute('returnValues', $returnValues);
    }

  }

  function evaluateExpression ($node) {

    if ($node instanceof Node\Expr\BinaryOp\Identical) {
      $valuesKnown = count(Value::get($node->left)) > 0 && count(Value::get($node->right)) > 0 ;
      $allSatisfied = true;
      foreach (Value::get($node->left) as $value1)
        foreach (Value::get($node->right) as $value2)
          if ($value1['type'] != 't_mixed' && $value2['type'] != 't_mixed')
            if ($value1['type'] != $value2['type'] || $value1['value'] !== $value2['value'])
              $allSatisfied = false;
      if ($valuesKnown && !$allSatisfied)
        $node->setAttribute('values', [['type' => 't_bool', 'value' => false]]);
    }

  }

  function registerSymbolsValues ($symbols, $values) {

    $node = $symbols;

    if ($symbols instanceof Node)
      $symbols = Symbol::get($symbols);

    foreach ($symbols as $symbol) {
      $this->symbolValues[$symbol] = [];
      foreach (Symbol::visibleScopes($symbol) as $scopeSymbol) {
        if (!isset($this->symbolValues[$scopeSymbol]))
          $this->symbolValues[$scopeSymbol] = [];
        foreach ($values as $value)
          $this->symbolValues[$scopeSymbol][Value::valueKey($value)] = $value;
      }
    }

  }

  function lookup ($symbol) {

    if (($symbol instanceof Node) && NodeConcept::isInvocationNode($symbol)) {

      $values = [];

      foreach (Symbol::get($symbol) as $symbolx)
        if (isset(context('code')->symbols[$symbolx]))
          foreach (context('code')->symbols[$symbolx]['definitionNodes'] as $definitionNode)
            if ($symbol->getAttribute('invocationTypes', []) == $definitionNode->getAttribute('types', []))
              if (NodeConcept::isExecutionContextNode($definitionNode))
                foreach ($definitionNode->getAttribute('returnValues', []) as $value)
                  $values[Value::valueKey($value)] = $value;

      return array_values($values);

    }

    if ($symbol instanceof Node) {
      $values = [];
      foreach (Symbol::get($symbol) as $symbolx)
        foreach ($this->lookup($symbolx) as $value)
          $values[Value::valueKey($value)] = $value;
      foreach (Value::literal($symbol) as $value)
        $values[Value::valueKey($value)] = $value;
      return array_values($values);
    }

    if (!$symbol)
      return [];

    $unqualifiedSymbol = Symbol::unqualified($symbol);

    foreach (Scope::visibleScopes(Scope::symbolScope($symbol)) as $visibleScope) {
      $lookupSymbol = Symbol::concat($visibleScope, $unqualifiedSymbol);
      if (isset($this->symbolValues[$lookupSymbol]))
        return array_values($this->symbolValues[$lookupSymbol]);
    }

    /**
     * Constants can fallback to same namespace and global namespace.
     * @see http://www.php.net/manual/en/language.namespaces.fallback.php#example-258
     */
    if (in_array(Symbol::symbolIdentifierGroup($symbol), ['constant'])) {

      if (isset(context('code')->symbols[$symbol]['values']))
        return context('code')->symbols[$symbol]['values'];

      foreach ([Scope::namespaceScope($symbol), ''] as $lookupScope) {
        $lookupSymbol = Symbol::concat($lookupScope, $unqualifiedSymbol);
        if (isset($this->symbolValues[$lookupSymbol]))
          return array_values($this->symbolValues[$lookupSymbol]);
      }
    }

    return [];

  }

  function evaluateArrayLiteral ($node) {

    if ($node instanceof Node\Expr\Array_)
      foreach ($node->items as $item) {

        if ($item->key) {
          $keys = [];
          foreach (Value::get($item->key) as $keyValue)
            $keys[] = [
              'type' => '',
              'value' => Value::convertToArrayKeyValue($keyValue['value']),
            ];
          if (count($keys) > 0)
            $item->key->setAttribute('values', $keys);
        }

      }

  }

  /**
   * Convert to a key value following PHP array key conversion rules.
   *
   * @see http://php.net/manual/en/language.types.array.php
   */
  static function convertToArrayKeyValue ($keyValue) {

    /**
     * Strings containing valid integers will be cast to the integer type.
     * E.g. the key "8" will actually be stored under 8. On the other
     * hand "08" will not be cast, as it isn't a valid decimal integer.
     */
    if (is_string($keyValue) && (((string) ((int) $keyValue)) === (string) $keyValue))
      return (int) $keyValue;

    /**
     * Floats are also cast to integers, which means that the fractional part
     * will be truncated. E.g. the key 8.7 will actually be stored under 8.
     */
    if (is_float($keyValue))
      return (int) floor($keyValue);

    /**
     * Bools are cast to integers, too, i.e. the key true will actually be
     * stored under 1 and the key false under 0.
     */
    if (is_bool($keyValue))
      return $keyValue ? 1 : 0;

    /**
     * Null will be cast to the empty string, i.e. the key null will
     * actually be stored under "".
     */
    if (is_null($keyValue))
      return '';

    return $keyValue;

  }

  static function valueKey ($value) {
    return $value['type'] . '/' . json_encode($value['value']);
  }

  static function literal ($node) {

    $values = [];

    if ($node instanceof Node\Arg)
      foreach (Value::literal($node->value) as $value)
        $values[] = $value;

    if ($node instanceof Node\Const_)
      foreach (Value::literal($node->value) as $value)
        $values[] = $value;

    if ($node instanceof Node\Expr\ConstFetch) {
      if (strtolower($node->name->toString()) == 'null')
        $values[] = [
          'type' => '',
          'value' => null,
        ];
      if (strtolower($node->name->toString()) == 'true')
        $values[] = [
          'type' => 't_bool',
          'value' => true,
        ];
      if (strtolower($node->name->toString()) == 'false')
        $values[] = [
          'type' => 't_bool',
          'value' => false,
        ];
    }

    if ($node instanceof Node\Scalar\DNumber)
      $values[] = [
        'type' => 't_float',
        'value' => $node->value,
      ];

    if ($node instanceof Node\Scalar\LNumber)
      $values[] = [
        'type' => 't_int',
        'value' => $node->value,
      ];

    if ($node instanceof Node\Scalar\String_) {
      $type = 't_string';
      if (is_numeric($node->value))
        $type = 't_autoFloat';
      if (((string) ((int) $node->value)) === (string) $node->value)
        $type = 't_autoInteger';
      $values[] = [
        'type' => $type,
        'value' => $node->value,
      ];
    }

    return $values;

  }

  /**
   * Get node analysis-time known values.
   * This function is meant to be used by rules and other inferences
   * as it implements some lightweight deduction logic.
   *
   * @param Node|string $node Values holding `Node` or a literal value.
   * @return mixed[]
   */
  static function get ($node) {

    if (is_string($node))
      return [[
        'type' => 't_string',
        'value' => $node,
      ]];

    $values = [];

    foreach ($node->getAttribute('values', []) as $value)
      $values[Value::valueKey($value)] = $value;

    if ($node instanceof Node\Arg)
      foreach (Value::get($node->value) as $value)
        $values[Value::valueKey($value)] = $value;

    if ($node instanceof Node\Expr\Ternary)
      foreach (array_merge(Value::get($node->if), Value::get($node->else)) as $value)
        $values[Value::valueKey($value)] = $value;

    if ($node instanceof Node\Param)
      if ((count($values) == 0) && $node->default instanceof Node)
        foreach (Value::get($node->default) as $value)
          $values[Value::valueKey($value)] = $value;

    foreach (Value::literal($node) as $value)
      $values[Value::valueKey($value)] = $value;

    return array_values($values);

  }

}
