<?php

namespace luka8088\phlint\rule;

use \luka8088\Phlint;
use \luka8088\phlint\Test as PhlintTest;

/**
 * Makes sure that @pure code is indeed pure.
 *
 * @see /documentation/attribute/pure.md
 */
class EnforcePureAttribute {

  function getIdentifier () {
    return 'enforcePureAttribute';
  }

  function getCategories () {
    return [
      'attribute',
      'default',
    ];
  }

  function getInferences () {
    return [
      'attribute',
      'purity',
    ];
  }

  function visitNode ($node) {
    if (in_array('pure', $node->getAttribute('attributes', []))
        && count(array_merge($node->getAttribute('purityBreaches', []), $node->getAttribute('isolationBreaches', []))) > 0)
      context('result')->addIssue(
        $node,
        'Function *' . $node->name . '* not pure.',
        array_merge($node->getAttribute('purityBreaches', []), $node->getAttribute('isolationBreaches', []))
      );
  }

}
