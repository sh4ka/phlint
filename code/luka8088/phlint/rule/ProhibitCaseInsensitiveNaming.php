<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * @see /documentation/rule/prohibitCaseInsensitiveNaming.md
 */
class ProhibitCaseInsensitiveNaming {

  function getIdentifier () {
    return 'prohibitCaseInsensitiveNaming';
  }

  function getCategories () {
    return [
      'default',
      'tidy',
    ];
  }

  function getInferences () {
    return [
      'symbol',
    ];
  }

  function visitNode ($node) {

    if (NodeConcept::isInvocationNode($node))
      $this->enforceRule($node, Symbol::get($node), $node->name);

    if ($node instanceof Node\Expr\New_)
      $this->enforceRule($node, Symbol::get($node), $node->class);

    if ($node instanceof Node\Stmt\Catch_)
      $this->enforceRule($node, Symbol::get($node->type), $node->type);

  }

  function enforceRule ($node, $symbols, $nameNode) {

    foreach ($symbols as $symbol)
      if (isset(context('code')->symbols[$symbol]))
        foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode) {
          if (!NodeConcept::isNamedNode($definitionNode))
            continue;
          $name = NodeConcept::displayPrint($nameNode);
          $definitionName = NodeConcept::displayPrint($definitionNode->name);
          $checkLength = min(strlen($name), strlen($definitionName));
          if ($checkLength > 0 && strtolower(substr($name, -$checkLength)) == strtolower(substr($definitionName, -$checkLength)) && substr($name, -$checkLength) != substr($definitionName, -$checkLength))
            context('result')->addIssue(
              $node,
              ucfirst(NodeConcept::referencePrint($node)) . ' is not using the same letter casing as ' .
                NodeConcept::referencePrint($definitionNode) . '.'
            );

        }

  }

  /**
   * Test case insensitive naming.
   *
   * @test @internal
   */
  static function unittest_caseInsensitiveNaming () {

    PhlintTest::assertIssues('

      namespace {

        function myFunction () {}
        myfunction();
        MYFUNCTION();
        myFunction();
        myFunCTion();

        function myParameters (int $i = 1, iNt $j = 2, INT $k = 3) {}

      }

      namespace myApp\myNamespace\mySubNamespace {
        class MyClass {}
      }

      namespace otherApp\otherNamespace\otherSubNamespace {
        use \myApp\myNamespace;
        use \myApp\myNamespace\mySubNamespace\MyClass;
        $x = new myNamespace\mySubNamespace\myclass();
        $x = new myNamespace\mySubNamespace\MYCLASS();
        $x = new myNamespace\mySubNamespace\MyClass();
        $x = new myNamespace\mySubNamespace\MyCLaSs();
        $x = new myclass();
        $x = new MYCLASS();
        $x = new MyClass();
        $x = new MyCLaSs();
      }

    ', [
      '
        Expression *myfunction()* is not using the same letter casing as function *myFunction()*
          on line 5.
      ',
      '
        Expression *MYFUNCTION()* is not using the same letter casing as function *myFunction()*
          on line 6.
      ',
      '
        Expression *myFunCTion()* is not using the same letter casing as function *myFunction()*
          on line 8.
      ',
      '
        Expression *new myNamespace\mySubNamespace\myclass()* is not using the same letter casing as class *MyClass*
          on line 21.
      ',
      '
        Expression *new myNamespace\mySubNamespace\MYCLASS()* is not using the same letter casing as class *MyClass*
          on line 22.
      ',
      '
        Expression *new myNamespace\mySubNamespace\MyCLaSs()* is not using the same letter casing as class *MyClass*
          on line 24.
      ',
      '
        Expression *new myclass()* is not using the same letter casing as class *MyClass*
          on line 25.
      ',
      '
        Expression *new MYCLASS()* is not using the same letter casing as class *MyClass*
          on line 26.
      ',
      '
        Expression *new MyCLaSs()* is not using the same letter casing as class *MyClass*
          on line 28.
      ',
    ]);
  }

  /**
   * Test indirections.
   * @test @internal
   */
  static function unittest_indirections () {

    $linter = PhlintTest::create();

    $linter->addSource('
      namespace A;
      class Z {
        function foo () {
          $x = new \B\Z();
          $x->bar();
        }
      }
    ');

    $linter->addSource('
      namespace B;
      class Z implements \B\I {
        function bar() {}
      }
      interface I {
        function bar();
      }
    ');

    $linter->addSource('
      namespace C;
      class Z {}
    ');

    $linter->addSource('
      namespace D;
      class Z {
        function baz (\B\I $x) {
          $x->bar();
        }
      }
    ');

    PhlintTest::assertNoIssues($linter->analyze('
      $x = new \A\Z();
      $x = new \C\Z();
      $x = new \D\Z();
    '));

  }

  /**
   * Test methods.
   * @test @internal
   */
  static function unittest_method () {
    PhlintTest::assertIssues('
      class A {
        function Foo () {}
      }
      class B {
        function Bar (A $a) {
          $a->foo();
        }
      }
    ', [
      'Expression *$a->foo()* is not using the same letter casing as method *function Foo()* on line 6.',
    ]);
  }

}
