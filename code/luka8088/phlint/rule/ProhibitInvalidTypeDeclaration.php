<?php

namespace luka8088\phlint\rule;

use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\phpLanguage;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;

/**
 * @see /documentation/rule/prohibitInvalidTypeDeclaration.md
 */
class ProhibitInvalidTypeDeclaration {

  function getIdentifier () {
    return 'prohibitInvalidTypeDeclaration';
  }

  function getCategories () {
    return [
      'default',
    ];
  }

  function getInferences () {
    return [
      'symbol',
      'templateSpecialization',
      'type',
    ];
  }

  function visitNode ($node) {

    if ($node instanceof Node\Stmt\Function_) {
      $this->checkType($node->returnType, $node);
      foreach ($node->params as $argument)
        $this->checkType($argument->type, $argument);
    }

  }

  function checkType ($type, $node) {

    if (is_string($type) && !in_array(strtolower($type), phpLanguage\Fixture::$typeDeclarationNonClassKeywords))
      context('result')->addIssue(
        $node,
        'Invalid type *' . $type . '* in declaration.'
      );

    if (is_object($type)) {
      $definitionNodes = [];
      foreach (Symbol::get($type) as $symbol)
        if (isset(context('code')->symbols[$symbol]))
          foreach (context('code')->symbols[$symbol]['definitionNodes'] as $definitionNode)
              $definitionNodes[] = $definitionNode;
      if (count($definitionNodes) == 0)
        context('result')->addIssue(
          $node,
          'Type declaration requires undefined ' . NodeConcept::referencePrint($type) . '.'
        );
    }

  }

  /**
   * Referencing a type that does not exist.
   * @test @internal
   */
  static function unittest_test () {
    PhlintTest::assertIssues('
      function foo (integer $i) : resource {}
      foo();
    ', [
      'Type declaration requires undefined *integer* on line 1.',
      'Type declaration requires undefined *resource* on line 1.',
    ]);
  }

}
