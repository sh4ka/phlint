<?php

namespace luka8088\phlint\rule;

use \ArrayObject;
use \luka8088\phlint\inference\Symbol;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node\Expr\ArrayDimFetch;
use \PhpParser\Node\Expr\Assign;
use \PhpParser\Node\Expr\AssignRef;
use \PhpParser\Node\Expr\Variable;
use \PhpParser\Node\Param;

/**
 * @see /documentation/rule/prohibitShortOpenTag.md
 */
class ProhibitShortOpenTag {

  function getIdentifier () {
    return 'prohibitShortOpenTag';
  }

  function getCategories () {
    return [
      'default',
      'legacy',
    ];
  }

  function visitNode ($node) {

    if ($node->getAttribute('hasShortOpenTag', false))
      context('result')->addIssue($node, 'Using short open tag is prohibited.');

  }

  /**
   * General rule test.
   *
   * @test @internal
   */
  static function unittest_test () {

    PhlintTest::assertIssues(trim('
      <?
      function foo () {}
    '), [
      'Using short open tag is prohibited on line 2.',
    ]);

  }

}
