<?php

namespace luka8088\phlint\rule;

use \luka8088\Phlint;
use \luka8088\phlint\NodeConcept;
use \luka8088\phlint\Test as PhlintTest;
use \PhpParser\Node;
use \PhpParser\Node\Name as NameNode;
use \PhpParser\Node\Stmt\Use_;
use \PhpParser\NodeVisitorAbstract;

/**
 * Prohibit `use` imports that are not actually been used by the code.
 */
class ProhibitUnusedImports extends NodeVisitorAbstract {

  function getIdentifier () {
    return 'prohibitUnusedImports';
  }

  function getCategories () {
    return [
      'tidy',
    ];
  }

  protected $imports = [];
  protected $usages = [];

  protected function resetState () {
    $this->imports = [];
    $this->usages = [];
  }

  function beforeTraverse(array $nodes) {
      $this->resetState();
  }

  function enterNode(Node $node) {

    if ($node instanceof Use_) {
      foreach ($node->uses as $import) {
        assert(!hasMember($this->imports, $import->alias));
        $this->imports[$import->alias] = $import;
      }
    }

    if ($node instanceof NameNode) {
      $this->usages[implode('\\', $node->parts)] = $node;
    }

    if ($node instanceof Node\Expr\Variable)
      foreach ($node->getAttribute('attributes', []) as $attribute)
        if (isset($attribute['name']) && $attribute['name'] == 'var' && isset($attribute['arguments'][0]))
          $this->usages[$attribute['arguments'][0]] = new Node\Name($attribute['arguments'][0]);

  }

  function afterTraverse (array $nodes) {

    foreach ($this->imports as $alias => $import) {

      $isImportUsed = false;

      $importAlias = substr(self::sourcePrint($import), strrpos(self::sourcePrint($import), '\\') + 1);

      if (hasMember($this->usages, $alias)) {
        $isImportUsed = true;
        continue;
      }

      foreach ($this->usages as $usage) {
        if ($usage->parts[0] == $import->alias) {
          $isImportUsed = true;
          break;
        }
        $useBaseAlias = substr(self::sourcePrint($usage), 0, strpos(self::sourcePrint($usage), '\\'));
        if ($importAlias == $useBaseAlias) {
          $isImportUsed = true;
          break;
        }
      }

      if (!$isImportUsed) {
        context('result')->addIssue($import, 'Import *' . NodeConcept::sourcePrint($import) . '* is not used.');
      }

    }
  }

  static function sourcePrint ($node) {
    if (is_string($node))
      return $node;
    $prettyPrinter = new \PhpParser\PrettyPrinter\Standard();
    return $prettyPrinter->prettyPrint([$node]);
  }

  /** @test @internal */
  static function unittest_test () {

    PhlintTest::assertNoIssues('
      use \a\b\c;
      $x = null;
      if ($x instanceof c) {}
    ');

    PhlintTest::assertIssues('
      use \a\b\c;
      $x = null;
      if ($x instanceof \a\b\c) {}
    ', [
      'Import *a\b\c* is not used on line 1.',
    ]);

    PhlintTest::assertNoIssues('
      namespace a\b\c {
        class d {}
      }
      namespace e {
        use \a\b;
        $x = new b\c\d();
      }
    ');

    PhlintTest::assertIssues('
      use \a\b;
      $x = new b\c\d();
    ', [
      // @todo: fix
      'Unable to invoke undefined *b\c\d* for the expression *new b\c\d()* on line 2.',
    ]);

    PhlintTest::assertNoIssues('
      namespace a {
        class B {}
      }
      namespace c {
        use a\B;
        class D {
          function e () {
            if (rand(0, 1))
              $x = new B();
          }
        }
      }

    ');

  }

  /**
   * Cross file import usage.
   *
   * @test @internal
   */
  static function unittest_crossFile () {

    $linter = Phlint::create();

    $linter->addSource('
      namespace a;
      class B {}
    ');

    PhlintTest::assertNoIssues($linter->analyze('
      namespace c;
      use \a\B;
      $x = new B();
    '));

  }

}
