<?php

function bbcode_add_element(resource $bbcode_container, string $tag_name, array $tag_rules) : bool {}
function bbcode_add_smiley(resource $bbcode_container, string $smiley, string $replace_by) : bool {}
function bbcode_create(array $bbcode_initial_tags = null) : resource {}
function bbcode_destroy(resource $bbcode_container) : bool {}
function bbcode_parse(resource $bbcode_container, string $to_parse) : string {}
function bbcode_set_arg_parser(resource $bbcode_container, resource $bbcode_arg_parser) : bool {}
function bbcode_set_flags(resource $bbcode_container, int $flags, int $mode = BBCODE_SET_FLAGS_SET) : bool {}
