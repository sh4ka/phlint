<?php

function bcompiler_load(string $filename) : bool {}
function bcompiler_load_exe(string $filename) : bool {}
function bcompiler_parse_class(string $class, string $callback) : bool {}
function bcompiler_read(resource $filehandle) : bool {}
function bcompiler_write_class(resource $filehandle, string $className, string $extends = '') : bool {}
function bcompiler_write_constant(resource $filehandle, string $constantName) : bool {}
function bcompiler_write_exe_footer(resource $filehandle, int $startpos) : bool {}
function bcompiler_write_file(resource $filehandle, string $filename) : bool {}
function bcompiler_write_footer(resource $filehandle) : bool {}
function bcompiler_write_function(resource $filehandle, string $functionName) : bool {}
function bcompiler_write_functions_from_file(resource $filehandle, string $fileName) : bool {}
function bcompiler_write_header(resource $filehandle, string $write_ver = '') : bool {}
function bcompiler_write_included_filename(resource $filehandle, string $filename) : bool {}
