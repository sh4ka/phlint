<?php

function exif_imagetype(string $filename) : int {}
function exif_read_data(string $filename, string $sections = null, bool $arrays = false, bool $thumbnail = false) : array {}
function exif_tagname(int $index) : string {}
function exif_thumbnail(string $filename, int &$width = 0, int &$height = 0, int &$imagetype = 0) : string {}
function read_exif_data($filename, $sections_needed, $sub_arrays, $read_thumbnail) {}
