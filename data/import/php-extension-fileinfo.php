<?php

function finfo_buffer(resource $finfo, string $string, int $options = FILEINFO_NONE, resource $context = null) : string {}
function finfo_close(resource $finfo) : bool {}
function finfo_file(resource $finfo, string $file_name, int $options = FILEINFO_NONE, resource $context = null) : string {}
function finfo_open(int $options = FILEINFO_NONE, string $magic_file = null) : resource {}
function finfo_set_flags(resource $finfo, int $options) : bool {}
function mime_content_type(string $filename) : string {}
