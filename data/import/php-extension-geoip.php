<?php

function geoip_asnum_by_name(string $hostname) : string {}
function geoip_continent_code_by_name(string $hostname) : string {}
function geoip_country_code3_by_name(string $hostname) : string {}
function geoip_country_code_by_name(string $hostname) : string {}
function geoip_country_name_by_name(string $hostname) : string {}
function geoip_database_info(int $database = GEOIP_COUNTRY_EDITION) : string {}
function geoip_db_avail(int $database) : bool {}
function geoip_db_filename(int $database) : string {}
function geoip_db_get_all_info() : array {}
function geoip_domain_by_name(string $hostname) : string {}
function geoip_id_by_name(string $hostname) : int {}
function geoip_isp_by_name(string $hostname) : string {}
function geoip_netspeedcell_by_name(string $hostname) : string {}
function geoip_org_by_name(string $hostname) : string {}
function geoip_record_by_name(string $hostname) : array {}
function geoip_region_by_name(string $hostname) : array {}
function geoip_region_name_by_code(string $country_code, string $region_code) : string {}
function geoip_setup_custom_directory(string $path) {}
function geoip_time_zone_by_country_and_region(string $country_code, string $region_code = '') : string {}
