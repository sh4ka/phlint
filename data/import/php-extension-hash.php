<?php

function hash(string $algo, string $data, bool $raw_output = false) : string {}
function hash_algos() : array {}
function hash_copy(resource $context) : resource {}
function hash_equals(string $known_string, string $user_string) : bool {}
function hash_file(string $algo, string $filename, bool $raw_output = false) : string {}
function hash_final(resource $context, bool $raw_output = false) : string {}
function hash_hmac(string $algo, string $data, string $key, bool $raw_output = false) : string {}
function hash_hmac_file(string $algo, string $filename, string $key, bool $raw_output = false) : string {}
function hash_init(string $algo, int $options = 0, string $key = null) : resource {}
function hash_pbkdf2(string $algo, string $password, string $salt, int $iterations, int $length = 0, bool $raw_output = false) : string {}
function hash_update(resource $context, string $data) : bool {}
function hash_update_file(resource $hcontext, string $filename, resource $scontext = null) : bool {}
function hash_update_stream(resource $context, resource $handle, int $length = -1) : int {}
