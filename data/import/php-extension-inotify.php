<?php

function inotify_add_watch(resource $inotify_instance, string $pathname, int $mask) : int {}
function inotify_init() : resource {}
function inotify_queue_len(resource $inotify_instance) : int {}
function inotify_read(resource $inotify_instance) : array {}
function inotify_rm_watch(resource $inotify_instance, int $watch_descriptor) : bool {}
