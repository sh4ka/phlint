<?php


class Lua
{
    function assign(string $name, string $value) {}
    function call(callable $lua_func, array $args = null, int $use_self = 0) {}
    function __call(callable $lua_func, array $args = null, int $use_self = 0) {}
    function __construct(string $lua_script_file) {}
    function eval(string $statements) {}
    function getVersion() : string {}
    function include(string $file) {}
    function registerCallback(string $name, callable $function) {}
}

class LuaClosure
{
    function __invoke($arg, ...$__variadic) : void {}
}
