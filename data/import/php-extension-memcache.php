<?php

function memcache_add() {}
function memcache_add_server() {}
function memcache_append() {}
function memcache_cas() {}
function memcache_close() {}
function memcache_connect() {}
function memcache_debug(bool $on_off) : bool {}
function memcache_decrement() {}
function memcache_delete() {}
function memcache_flush() {}
function memcache_get(string $key, int &$flags) {}
function memcache_get_extended_stats() {}
function memcache_get_server_status() {}
function memcache_get_stats() {}
function memcache_get_version() {}
function memcache_increment() {}
function memcache_pconnect() {}
function memcache_prepend() {}
function memcache_replace() {}
function memcache_set() {}
function memcache_set_compress_threshold() {}
function memcache_set_failure_callback() {}
function memcache_set_sasl_auth_data() {}
function memcache_set_server_params() {}

class Memcache
{
    function add(string $key, $var, int $flag = 0, int $expire = 0) : bool {}
    function addServer(string $host, int $port = 11211, bool $persistent = false, int $weight = 0, int $timeout = 0, int $retry_interval = 0, bool $status = false, callable $failure_callback = null, int $timeoutms = 0) : bool {}
    function close() : bool {}
    function connect(string $host, int $port = 0, int $timeout = 0) : bool {}
    function decrement(string $key, int $value = 1) : int {}
    function delete(string $key, int $timeout = 0) : bool {}
    function flush() : bool {}
    function get(string $key, int &$flags = 0) : string {}
    function getExtendedStats(string $type = '', int $slabid = 0, int $limit = 100) : array {}
    function getServerStatus(string $host, int $port = 11211) : int {}
    function getStats(string $type = '', int $slabid = 0, int $limit = 100) : array {}
    function getVersion() : string {}
    function increment(string $key, int $value = 1) : int {}
    function pconnect(string $host, int $port = 0, int $timeout = 0) {}
    function replace(string $key, $var, int $flag = 0, int $expire = 0) : bool {}
    function set(string $key, $var, int $flag = 0, int $expire = 0) : bool {}
    function setCompressThreshold(int $threshold, float $min_savings = null) : bool {}
    function setServerParams(string $host, int $port = 11211, int $timeout = 0, int $retry_interval = false, bool $status = false, callable $failure_callback = null) : bool {}
}

class MemcachePool
{
}
