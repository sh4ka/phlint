Phlint - PHP Linter, Code Analyzer and Tester
=============================================

Introduction
------------

Phlint is a tool with an aim to help maintain quality of php code by analyzing code and pointing out potential code
issues. It focuses on how the code works rather than how the code looks. Phlint is designed from the start to do
deep semantic analysis rather than doing only shallow or stylistic analysis.

Basic usage
-----------

Phlint can be downloaded from [the download section](https://bitbucket.org/luka8088/phlint/downloads) and used
as a command line utility:

    # To download run:
    wget https://bitbucket.org/luka8088/phlint/downloads/phlint-0.2.6.phar -O phlint.phar

    # To invoke run:
    php phlint.phar /path/to/project

Alternatively it can be included in the project through composer:

    # To install run:
    composer require luka8088/phlint

    # To invoke run:
    ./vendor/bin/phlint

Documentation
-------------

For a full documentation visit [Phlint documentation page](https://bitbucket.org/luka8088/phlint/src/master/documentation/index.md).

License
-------

Phlint is licensed under the [MIT license](https://bitbucket.org/luka8088/phlint/src/master/license.txt).
