<?php

use \luka8088\phlint\Test as PhlintTest;

class ConditionalGuaranteeBarrierTest {

  /**
   * Test conditional guarantees barrier.
   * @test @internal
   */
  static function unittest_conditionGuaranteesBarrier () {

    PhlintTest::assertNoIssues('
      function f () {
        $list = isset($undefinedVariable) ? [1, 2] : [];
        foreach ($list as $listItem)
          $result = $listItem;
        if (empty($result))
          throw new \Exception("No result");
        return $result;
      }
    ');

    PhlintTest::assertNoIssues('
      function f () {
        $list = isset($undefinedVariable) ? [1, 2] : [];
        foreach ($list as $listItem)
          $result = $listItem;
        if (empty($result["x"]))
          throw new \Exception("No result");
        return $result["y"];
      }
    ');

    PhlintTest::assertNoIssues('
      function f () {
        $list = isset($undefinedVariable) ? [1, 2] : [];
        foreach ($list as $listItem)
          $result = $listItem;
        if (!isset($result))
          throw new \Exception("No result");
        return $result;
      }
    ');

    PhlintTest::assertNoIssues('
      function f () {
        $list = isset($undefinedVariable) ? [1, 2] : [];
        foreach ($list as $listItem)
          $result = $listItem;
        if (!isset($result["x"]))
          throw new \Exception("No result");
        return $result["y"];
      }
    ');

  }

}
