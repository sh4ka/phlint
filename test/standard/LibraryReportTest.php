<?php

use \luka8088\phlint\Test as PhlintTest;

/**
 * These tests deal with how library issues are reported.
 * Some issues occur because of the way library is being used while some issues
 * occur because the library itself has issues. In general we want to report
 * the former but not the latter.
 */
class LibraryReportTest {

  /**
   * Test that library issues are not being reported as usage issues.
   * @test @internal
   */
  static function unittest_dontReportLibraryIssues () {

    $linter = PhlintTest::create();

    $linter->addSource('
      function foo ($a, $b = "!") {
        $x = $y;
        return $a + $b;
      }
    ', true);

    PhlintTest::assertIssues($linter->analyze('
      foo();
      foo(1);
      foo("bar");
    '), [
      '
        Provided variable *$a* of type *string* is not compatible in the expression *$a + $b* on line 3.
          Trace #1: Function *foo("bar", "!")* specialized for the expression *foo("bar")* on line 3.
      ',
    ]);

  }

}
