<?php

use \luka8088\phlint\Test as PhlintTest;

class MixedTypeTest {

  /**
   * Test invocation on mixed type.
   * @test @internal
   */
  static function unittest_mixedInvocation () {
    PhlintTest::assertNoIssues('
      function foo (ArrayObject $a) {
        $a["x"]->baz();
      }
    ');
  }

}
