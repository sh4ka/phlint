<?php

use \luka8088\phlint\Test as PhlintTest;

class PurityTest {

  /**
   * Test enforcement of purity.
   * @test @internal
   */
  static function unittest_test () {

    PhlintTest::assertNoIssues('
      function foo () {
        bar();
      }
      function bar () {
        $x = $GLOBALS["x"];
        static $i = 0;
      }
    ');

    PhlintTest::assertNoIssues('
      /** @pure */
      function foo () {
        return bar();
      }
      function bar () {
        return 2;
      }
    ');

    PhlintTest::assertIssues('
      /** @pure */
      function foo () {
        static $i = 0;
      }
      foo();
    ', [
      '
        Function *foo* not pure on line 2.
          Cause #1: Declaring static variable *$i* on line 3.
      ',
    ]);

    PhlintTest::assertIssues('
      /** @pure */
      function foo () {
        return __dir__ . __file__ . __line__;
      }
    ', [
      '
        Function *foo* not pure on line 2.
          Cause #1: Using magic constant *__DIR__* on line 3.
          Cause #2: Using magic constant *__FILE__* on line 3.
          Cause #3: Using magic constant *__LINE__* on line 3.
      ',
    ]);

    PhlintTest::assertIssues('
      /** @pure */
      function foo () {
        static $i = 0;
        $i += 1;
        return $i;
      }
    ', [
      '
        Function *foo* not pure on line 2.
          Cause #1: Declaring static variable *$i* on line 3.
      ',
    ]);

    PhlintTest::assertIssues('
      /** @pure */
      function foo () {
        bar();
      }
      function bar () {
        baz();
      }
      function baz () {
        $x = $GLOBALS["x"];
      }
    ', [
      '
        Function *foo* not pure on line 2.
          Cause #1: Calling non-isolated function *bar()* on line 3.
            Cause #1: Calling non-isolated function *baz()* on line 6.
              Cause #1: Accessing superglobal *$GLOBALS* on line 9.
      ',
    ]);

    PhlintTest::assertIssues('
      /** @pure */
      function foo () {
        return A::$i + 1;
      }
      class A {
        static $i = 0;
      }
    ', [
      '
        Function *foo* not pure on line 2.
          Cause #1: Accessing global variable *A::$i* on line 3.
      ',
    ]);

    PhlintTest::assertIssues('
      /** @pure */
      function foo () {
        setlocale(LC_ALL, "en_GB");
        $x = [5, 3, 1, 2, 4];
        usort($x, function ($a, $b) {
          return $a - $b;
        });
        return substr(implode("", $x), 2, 2);
      }
    ', [
      '
        Function *foo* not pure on line 2.
          Cause #1: Calling non-isolated function *setlocale(LC_ALL, "en_GB")* on line 3.
            Cause #1: Modifies global state.
      ',
    ]);

  }

}
