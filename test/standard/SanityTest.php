<?php

use \luka8088\phlint\Test as PhlintTest;

class SanityTest {

  /**
   * Regression test for:
   * Error:
   *   Call to a member function getAttribute() on null
   * Code:
   *   if ($node instanceof Node\Expr\List_)
   *     foreach ($node->vars as $variableNode)
   *       $variableNode->getAttribute('foo');
   *
   * @see http://www.php.net/manual/en/function.list.php
   *
   * @test @internal
   */
  static function unittest_emptyListVariable () {
    PhlintTest::assertNoIssues('
      list(, $foo) = explode(" ", "Hello world");
    ');
  }

}
