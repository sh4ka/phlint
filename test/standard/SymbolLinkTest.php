<?php

use \luka8088\phlint\Test as PhlintTest;

class SymbolLinkTest {

  /**
   * Test linking to an extended interface.
   * @test @internal
   */
  static function unittest_linkToExtendedInterface () {

    PhlintTest::assertIssues('
      class C {
        function foo () {}
      }
      $x = new C();
      $y = $x;
      $y->foo();
      $y->bar();
    ', [
      'Unable to invoke undefined *C::bar* for the expression *$y->bar()* on line 7.',
    ]);

    PhlintTest::assertIssues('
      namespace e\f {
        use \c;
        function bar (c\d\J $x) {
          $x->foo();
          $x->baz();
        }
      }
      namespace c\d {
        use \a\b;
        interface J extends b\I {}
      }
      namespace a\b {
        interface I {
          function foo () {}
        }
      }
    ', [
      'Unable to invoke undefined *c\d\J::baz* for the expression *$x->baz()* on line 5.',
    ]);

  }

  /**
   * Test invoking a array element doesn't get linked to an invalid definition
   * causing an infinite recursion in template specialization.
   * @test @internal
   */
  static function unittest_invokeArrayElement () {

    PhlintTest::assertNoIssues('
      namespace a {
        class B {
          function foo () {
            $this->bar["helper"]();
          }
        }
      }
    ');

  }

  /**
   * Test static call linking with relative reference.
   * @test @internal
   */
  static function unittest_staticCallWithRelativeReference () {

    PhlintTest::assertIssues('
      namespace a {
        class D {
          static function foo () { return 1; }
        }
      }
      namespace {
        use \a as c;
        function baz () {
          return c\D::foo();
          return c\D::bar();
        }
      }
    ', [
      'Unable to invoke undefined *a\D::bar* for the expression *c\D::bar()* on line 10.',
    ]);

  }

  /**
   * @test @internal
   * Test nested extends linking.
   */
  static function unittest_nestedExtends () {
    PhlintTest::assertIssues('
      trait T {
        function Foo () {}
      }
      class A {
        use T;
      }
      class B extends A {}
      class C extends B {}
      class D extends B {}

      $o = new D();
      $o->Foo();
      $o->Bar();
    ', [
      'Unable to invoke undefined *D::Bar* for the expression *$o->Bar()* on line 13.',
    ]);
  }

}
