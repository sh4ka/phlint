<?php

use \luka8088\phlint\Test as PhlintTest;

class VariableInitializationTest {

  /**
   * That variable initialization for conditional connectives.
   * @test @internal
   */
  static function unittest_conditionalConnectives () {
    PhlintTest::assertIssues('
      function foo () {
        $a = ($b = rand(0, 1)) && ($c = $b);
        $d = $c;
      }
    ', [
      'Variable *$c* used before initialized on line 3.',
    ]);
  }

}
